import {ApolloServer, } from "apollo-server-express";
import {AuthChecker, buildSchema} from "type-graphql";
import Resolvers from "../src/resolvers";
import {Context} from "apollo-server-core";
import {Request} from "express";

type role = "ADMIN" | "TEACHER";


type ReqContext = {
    req: Request
}

const customAuthChecker: AuthChecker<any,role> = (
    { root, args, context, info },
    roles,
) => {
    const jwt = context.headers.authorization;
    console.log("jwt", jwt);
    // here we can read the user from context
    // and check his permission in the db against the `roles` argument
    // that comes from the `@Authorized` decorator, eg. ["ADMIN", "MODERATOR"]

    return true; // or false if access is denied
};
const schema = buildSchema({
    resolvers: Resolvers,
    authChecker: customAuthChecker,
});

const server = new ApolloServer({
    // @ts-ignore
    schema,
    context: (context: Context<ReqContext>) => ({ req: context.req }),
    playground: {
        settings: {
            'editor.theme': 'dark',
        },
    },
});

export default server;

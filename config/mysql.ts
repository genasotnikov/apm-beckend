import "reflect-metadata";
import {getConnectionManager} from "typeorm";
import {
        UserValue,
        LessonTypeValue,
        LessonValue,
        ScheduleValue,
        SemesterValue,
        LessonInScheduleValue,
        SubgroupValue,
        GroupValue,
        ProfileValue,
        FacultyValue,
        SubgroupLessonValue,
        OverrideDayValue
} from "../src/Models";
import Department from "../src/Models/Department";
import {ApolloError} from "apollo-server-errors";

export const connectToDB = async () => {
        try {
                const connectionManager = getConnectionManager();
                const dbConnection = await connectionManager.create({
                        name: Math.random().toString(),
                        type: "mysql",
                        host: "remotemysql.com",
                        port: 3306,
                        username: "STrq3A7RvK",
                        password: "zv2dOUsDDW",
                        database: "STrq3A7RvK",
                        timezone: "+0300",
                        entities: [
                                UserValue,
                                LessonTypeValue,
                                LessonValue,
                                ScheduleValue,
                                SemesterValue,
                                LessonInScheduleValue,
                                SubgroupValue,
                                GroupValue,
                                ProfileValue,
                                FacultyValue,
                                SubgroupLessonValue,
                                Department,
                                OverrideDayValue
                        ],
                        logging: true,
                        migrations: ["/migrations/*.ts"],
                        cli: {
                                migrationsDir: "migrations"
                        }
                });
                return dbConnection.connect();
        }
        catch (e) {
                throw new ApolloError(e, "500");
        }

};

import {Field, ID, ObjectType} from "type-graphql";
import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@ObjectType()
@Entity({name: "faculties"})
class Faculty {
    @Field(type => ID)
    @PrimaryGeneratedColumn("increment")
    private id: number;

    @Column()
    @Field()
    private name:string;

    @Column()
    @Field()
    private shortness: string;

    setValue(name:string, shortness: string) {
        this.name = name;
        this.shortness = shortness;
    }

    get getShortness():string {
        return this.shortness;
    }
}

export default Faculty;
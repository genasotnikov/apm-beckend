import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Field, ID, ObjectType} from "type-graphql";
import {LessonType, LessonValue, ScheduleType, ScheduleValue, SemesterType, SemesterValue} from "../../Models";

@ObjectType()
@Entity("lessons_in_schedule")
class LessonInSchedule {
    @Field(() => ID)
    @PrimaryGeneratedColumn("increment")
    readonly id: number;

    @Field()
    @Column({type: "int"})
    private dayIndex: number;

    get getDayIndex():number {
        return this.dayIndex;
    }

    @Column({type: "int"})
    public lessonId: number;

    @Field()
    @Column({type: "int"})
    private weekIndex: number;

    get getWeekIndex() {
        return this.weekIndex;
    }

    @Column({type: "int"})
    private scheduleId: number;

    @Field(() => Boolean)
    @Column({type: "boolean"})
    private isChisl: boolean;

    get getIsChisl():boolean {
        return this.isChisl;
    }

    @Field(type => ScheduleValue)
    private schedule: ScheduleType;

    @Field(() => LessonValue)
    private lesson: LessonType;

    get getLesson(): LessonType {
        return this.lesson
    }

    setValue(dayIndex: number, weekIndex:number, lessonId:number, scheduleId:number, isChisl: boolean) {
        this.dayIndex = dayIndex;
        this.weekIndex = weekIndex;
        this.lessonId = lessonId;
        this.scheduleId = scheduleId;
        this.isChisl = isChisl;
    }

    setLesson(lesson:LessonType) {
        this.lesson = lesson;
    }

    get getscheduleId(): number {
        return this.scheduleId;
    }

    setSchedule(sch: ScheduleType) {
        this.schedule = sch;
    }
}

export default LessonInSchedule;

import {Field, ObjectType, ID} from "type-graphql";
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { LessonTypeValue } from "../index";
import {LessonTypeType, UserValue} from "../../Models";



@Entity("lessons")
@ObjectType()
class Lesson {
    @Field(() => ID)
    @PrimaryGeneratedColumn("increment")
    id: number;

    @OneToOne(() => UserValue)
    @Column({ type: "int" })
    teacherId: number;

    @Column({ type: "varchar" })
    @Field()
    content: string;

    @OneToOne(() => LessonTypeValue)
    @Column({ type: "int" })
    typeId: number;

    @Field(() => LessonTypeValue)
    type: LessonTypeType;

    public setLessonType(lessonType:LessonTypeType) {
        this.type = lessonType;
    }

    public setValue(teacherId: number, content: string, typeId: number) {
        this.teacherId = teacherId;
        this.content = content;
        this.typeId = typeId;
    }
}

export default Lesson;
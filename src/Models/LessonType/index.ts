import {Entity, Column, PrimaryGeneratedColumn, OneToOne, Index} from "typeorm";
import {ObjectType, Field, ID, Float} from "type-graphql";
import { UserValue } from "../../Models";

@ObjectType()
@Entity("lesson_types")
@Index(["teacherId", "name"], { unique: true })
class LessonType {
    @Field(type => ID)
    @PrimaryGeneratedColumn("increment")
    readonly id: number;

    @OneToOne(type => UserValue)
    @Column()
    private teacherId: number;

    @Field(type => String)
    @Column()
    private name: string;

    @Field(type => Float)
    @Column({type: "float"})
    private time: number;

    public setValue(name:string, teacherId: number, time: number) {
        this.name = name;
        this.teacherId = teacherId;
        this.time = time;
    }
}

export default LessonType;
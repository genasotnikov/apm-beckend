import {Field, ID, Int, ObjectType} from "type-graphql";
import {Column, Entity, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {FacultyType, FacultyValue} from "../../Models";

@ObjectType()
@Entity("profiles")
class Profile {
    @Field(() => ID)
    @PrimaryGeneratedColumn("increment")
    id: number;

    @Column()
    @Field()
    private name:string;

    @Column()
    @Field()
    private shortness: string;

    get getShortness() { return this.shortness }

    @OneToOne(() => FacultyValue)
    @Column({ type: "int" })
    @Field(() => Int)
    facultyId: number;

    @Field(() => FacultyValue)
    faculty: FacultyType;

    set setFaculty(faculty: FacultyType) {
        this.faculty = faculty;
    }

    public setValues(name: string, shortness: string) {
        this.name = name;
        this.shortness = shortness;
    }

    public getGroupShortness(year: number) {
        return year+this.shortness;
    }
}

export default Profile;
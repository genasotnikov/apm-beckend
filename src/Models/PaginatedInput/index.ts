import {Field, InputType, Int} from "type-graphql";

@InputType()
class PaginatedInput {
    @Field(() => Int, { nullable: true })
    lastIndex: number;

    @Field(() => Int, { nullable: true })
    limit: number;
}

export default PaginatedInput;

import { LessonValue, LessonType } from "../";
import {Field, ID, ObjectType} from "type-graphql";

@ObjectType()
class Day {
    @Field(type => ID)
    teacherId: string

    @Field()
    date: Date

    @Field(type => [LessonValue])
    lessons: LessonType[]
}
type SubgroupLessonOfVedomost = {
    dayIndex: number;
    yearProfile: string;
    groupSubgroup: string;
    faculty: string;
    content: string;
    lessonType: string;
}

export default SubgroupLessonOfVedomost

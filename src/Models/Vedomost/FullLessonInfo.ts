import {Field, Int, ObjectType} from "type-graphql";
import SubgroupLessonOfVedomost from "./SubgroupLessonOfVedomost";

@ObjectType()
class FullLessonInfo {
    @Field(() => Int)
    private dayIndex: number;

    get getDayIndex() {
       return this.dayIndex;
    }

    @Field(() => String)
    private yearProfile: string;

    @Field(() => [String])
    private groupSubgroups: string[];

    @Field(() => String)
    private faculty: string;

    @Field(() => String)
    private content: string;

    @Field(() => String)
    private lessonType: string;

    setValue = (data: SubgroupLessonOfVedomost) => {
        this.dayIndex = data.dayIndex;
        this.content = data.content;
        this.faculty = data.faculty;
        this.groupSubgroups = [data.groupSubgroup];
        this.lessonType = data.lessonType;
        this.yearProfile = data.yearProfile;
    }

    addSubgroup = (data: string) => {
        this.groupSubgroups.push(data);
    }
}

export default FullLessonInfo;

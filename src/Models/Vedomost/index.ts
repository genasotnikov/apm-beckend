import {Field, Int, ObjectType} from "type-graphql";
import GroupInfo from "./GroupInfo";
import LessonInSchedule from "../LessonInShedule";
import {SubgroupLessonType} from "../index";
import SubgroupLessonLogic from "../../Logic/SubgroupLesson";

@ObjectType()
export class VedomostDay {
    @Field()
    private date: string;

    @Field(() => [VedomostLesson])
    private lessons: VedomostLesson[] = new Array<VedomostLesson>();

    async setValues(iterableDate: Date, lessonsOfTheDay: LessonInSchedule[]) {
        for (const lotd of lessonsOfTheDay) {
            const vedLes = new VedomostLesson();
            const groupInfo = new GroupInfo();
            const subgroupLessons = (await new SubgroupLessonLogic().findSubgroupLessons({ lesson_in_schedule_id: lotd.id }) as SubgroupLessonType[])
                .map(el => el.getSubgroup);
            groupInfo.setValue(subgroupLessons);
            vedLes.setValues(lotd, groupInfo);
            this.lessons.push(vedLes);
        }
        this.date = `${iterableDate.getDate()}.${iterableDate.getMonth()}.${iterableDate.getFullYear()}`;
    }
};

@ObjectType()
export class VedomostLesson {
    // LessonInSchedule
    @Field()
    private dayIndex: number;
    // Lesson
    @Field()
    private content: string;
    @Field()
    private typeId: number;

    // Group
    @Field(() => GroupInfo)
    private groupInfo: GroupInfo;

    setValues (lesson: LessonInSchedule, groupInfo: GroupInfo) {
        this.dayIndex = lesson.getDayIndex;
        this.content = lesson.getLesson.content;
        this.typeId = lesson.getLesson.typeId;
        this.groupInfo = groupInfo;
    }
}

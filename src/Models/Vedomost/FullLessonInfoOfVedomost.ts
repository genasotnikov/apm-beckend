import {Field, Int, ObjectType} from "type-graphql";
import FullLessonInfo from "./FullLessonInfo";

@ObjectType()
class FullDayInfoOfVedomost {
    @Field(() => Date)
    date: Date

    @Field(() => [FullLessonInfo])
    lessonsData: FullLessonInfo[];

    setValue = (date: Date = null, lessonsData: FullLessonInfo[] = null) => {
        if (!(date && lessonsData)) throw new Error("Internal server error")
        else {
            this.date = date;
            this.lessonsData = lessonsData;
        }
    }
}

export default FullDayInfoOfVedomost;

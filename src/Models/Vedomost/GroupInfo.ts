import {Field, Int, ObjectType} from "type-graphql";
import Subgroup from "../Subgroup";

@ObjectType()
class VedomostGroup {
    @Field()
    group: number

    @Field(() => [Int])
    subgroups: number[]
}

@ObjectType()
class VedomostProfile {
    @Field()
    profile: string
    @Field(() => [VedomostGroup])
    groups: VedomostGroup[]
}

interface GroupInfoInterface {
    faculties: string[];
    year: number;
    groups: VedomostProfile[];
}

@ObjectType()
class GroupInfo implements GroupInfoInterface {
    @Field(() => [String])
    public faculties: string[]; // shortness.toLowerCase()
    @Field(() => Int)
    public year: number; // year.toString()+profile.shortness.toLowerCase();
    @Field(() => VedomostProfile)
    public groups: VedomostProfile[];

    setValue(subgroups: Subgroup[]) {
        this.faculties = subgroups.reduce((accumulator, subgroup) => {
            const facultyValue = subgroup.getGroup.getProfile.faculty.getShortness.toLowerCase();
            if (accumulator.find((exValue) => exValue === facultyValue)) {
                return [...accumulator];
            }
            return [...accumulator, facultyValue];}, []);
        this.year = (new Date()).getFullYear() - Number(subgroups[0].getGroup.year);
        this.groups = subgroups.reduce((accumulator: VedomostProfile[], subgroup) => {
            const accumCopy = accumulator ? [...accumulator] : [];
            const profile = subgroup.getGroup.getProfile.getShortness;
            const group = Number(subgroup.getGroup.number);
            const subgroupNumber = subgroup.getNumber;
            const recInd1 = accumulator.findIndex((el) => el.profile === profile);
            const vedGroup: VedomostGroup = {
                group,
                subgroups: [subgroupNumber]
            };
            if (recInd1 !== -1) {
                const recInd2 = accumulator[recInd1].groups.findIndex((gr) => gr.group === group);
                if (recInd2 !== -1) {
                    accumCopy[recInd1].groups[recInd2].subgroups =
                        [...accumCopy[recInd1].groups[recInd2].subgroups, subgroupNumber];
                    return [...accumulator, ...accumCopy];
                } else {
                    accumCopy[recInd1].groups = [...accumCopy[recInd1].groups, vedGroup];
                    return [...accumulator, ...accumCopy];
                }
            } else {
                const newObj:VedomostProfile = {
                    profile,
                    groups: [vedGroup],
                };
                return [...accumulator, newObj]
            }
        }, []);
    }
}

export default GroupInfo;

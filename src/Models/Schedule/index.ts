import {Column, Entity, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import { SemesterValue, UserValue } from "../../Models";
import {Field, ID, Int, ObjectType} from "type-graphql";

@ObjectType()
@Entity({name: "schedules"})
class Schedule {
    @Field(() => ID)
    @PrimaryGeneratedColumn("increment")
    id: number;

    @Field(() => Int)
    @OneToOne(type => SemesterValue)
    @Column()
    semesterId: number;

    @Field(() => Int)
    @OneToOne(type => UserValue)
    @Column()
    teacherId: number;

    @Field(() => Boolean)
    @Column({ type: "boolean" })
    isFinished: boolean;

    setValue(semesterId:number, teacherId:number, isFinished:boolean) {
        this.semesterId = semesterId;
        this.teacherId = teacherId;
        this.isFinished = isFinished;
    }
}

export default Schedule;

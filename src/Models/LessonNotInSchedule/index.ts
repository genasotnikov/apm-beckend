import {Field, ID} from "type-graphql";
import {Column, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {LessonType, LessonValue, OverrideDayType, OverrideDayValue} from "../../Models";

class LessonNotInSchedule {

    setValue = (dayId: number, dayIndex: number, lessonId: number) => {
        this._dayId = dayId;
        this._dayIndex = dayIndex;
        this._lessonId = lessonId;
    }

    @Field(() => ID)
    @PrimaryGeneratedColumn("increment")
    readonly id: number;

    @OneToOne(type => OverrideDayValue)
    @Column({ name: "day_id" })
    private _dayId: number;

    @Field(() => OverrideDayValue)
    day: OverrideDayType;

    @Column({ name: "day_index" })
    private _dayIndex: number;

    @OneToOne(type => LessonValue)
    @Column({ name: "lesson_id" })
    private _lessonId: number;

    @Field(()=> LessonValue)
    lesson: LessonType;
}

export default LessonNotInSchedule;

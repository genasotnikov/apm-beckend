import {Column, Entity, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Field, ID, ObjectType} from "type-graphql";
import {TeacherType, TeacherValue, UserValue} from "../index";

@ObjectType()
@Entity("override_days")
class OverrideDay {

    get date() {
        return this._date;
    }

    setValue = (date: Date, teacherId: number) => {
        this._date = date;
        this._teacherId = teacherId;
    }

    @Field(() => ID)
    @PrimaryGeneratedColumn("increment")
    readonly id: number;

    @OneToOne(type => UserValue)
    @Column({ name: "teacherId" })
    private _teacherId: number;

    @Field(() => ID, { name: "date" })
    @Column({ type: "date", name: "date" })
    private _date: Date;
}

export default OverrideDay;

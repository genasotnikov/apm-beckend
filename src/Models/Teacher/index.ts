import {Field, ObjectType} from "type-graphql";
import Department from "../Department";

@ObjectType()
class Teacher {
    @Field()
    readonly firstName: string;

    @Field({nullable: true})
    readonly middleName: string;

    @Field()
    readonly lastName: string;

    @Field()
    readonly department: Department;

    @Field()
    readonly hasSchedule: boolean;


    constructor(firstName: string, middleName: string|null = null, lastName: string, hasSchedule?: boolean, department?: Department) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        if (hasSchedule !== undefined && hasSchedule !== null) this.hasSchedule = hasSchedule;
        this.department = department;
    }
}

export default Teacher;
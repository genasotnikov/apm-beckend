import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import { GroupValue, GroupType } from "../../Models";
import {Field, ID, Int, ObjectType} from "type-graphql";

@ObjectType()
@Entity("subgroups")
class Subgroup {
    @Field(() => ID)
    @PrimaryGeneratedColumn("increment")
    private id:number;

    @Field(() => Int)
    @Column({ type: "int" })
    private number: number;

    get getNumber():number {
        return this.number
    }

    @Column({ type: "int" })
    private groupId: number;

    get getGroupId () {
        return this.groupId;
    }

    @Field(() => GroupValue)
    private group: GroupType;

    get getGroup():GroupType {
        return this.group
    }

    set setGroup (group: GroupType) {
        this.group = group;
    }
}

export default Subgroup;
import {Field, InputType, Int} from "type-graphql";
import {GroupInDb, formOfEdu, typeOfEdu} from "./GroupInDb";

@InputType()
class GroupInput implements GroupInDb {

    @Field(() => String)
    formOfEdu: formOfEdu;

    @Field(() => Int)
    number: number;

    @Field(() => Int)
    profileId: number;

    @Field(() => String)
    typeOfEdu: typeOfEdu;

    @Field(() => Int)
    year: number;
}

export default GroupInput;

import {Field, ID, Int, ObjectType} from "type-graphql";
import {ProfileType, ProfileValue} from "../index";
import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {GroupInDb, formOfEdu, typeOfEdu} from "./GroupInDb";

@ObjectType()
@Entity({name: "groups"})
class Group implements GroupInDb {
    @Field(() => Int)
    @PrimaryGeneratedColumn("increment")
    private id: number;

    @Field(() => Int)
    @OneToOne(() => ProfileValue)
    @Column()
    profileId: number;

    get getProfileId():number {
        return this.profileId;
    }

    @Field(() => ProfileValue)
    private profile: ProfileType;

    get getProfile():ProfileType {
        return this.profile;
    }

    set setProfile(pr:ProfileType) {
        this.profile = pr;
    }

    @Column()
    @Field(() => Int)
    year: number;

    @Column({name: "type_of_edu"})
    @Field()
    typeOfEdu: typeOfEdu;

    @Column({name: "form_of_edu"})
    @Field()
    formOfEdu: formOfEdu;

    @Column({ type: "int" })
    @Field()
    number: number;

    public setValue({profileId, year, number, formOfEdu, typeOfEdu }: GroupInDb) {
        this.profileId = profileId;
        this.year = year;
        this.number = number;
        this.formOfEdu = formOfEdu;
        this.typeOfEdu = typeOfEdu;
    }
}

export default Group;
export type formOfEdu = "ОЧНАЯ" | "ЗАОЧНАЯ";
export type typeOfEdu = "АСПИРАНТУРА" | "БАКАЛАВРИАТ" | "МАГИСТРАТУРА";

export interface GroupInDb {
    profileId: number;
    year: number;
    number: number;
    formOfEdu: formOfEdu;
    typeOfEdu: typeOfEdu;
}

import UserImport from "./User";
import TeacherImport from "./Teacher";
import ProfileImport from "./Profile";
import FacultyImport from "./Faculty";
import LessonTypeImport from "./LessonType";
import LessonImport from "./Lesson";
import SemesterImport from "./Semester";
import ScheduleImport from "./Schedule";
import SubgroupLessonImport from "./SubgroupLesson";
import LessonInScheduleImport from "./LessonInShedule";
import GroupImport from "./Group";
import SubgroupImport from "./Subgroup";
import OverrideDayImport from "./OverrideDay";
import LessonNotInScheduleImport from "./LessonNotInSchedule";
import PaginatedInputImport from "./PaginatedInput";


export type UserType = UserImport;
export const UserValue = UserImport;

export type TeacherType = TeacherImport;
export const TeacherValue = TeacherImport;

export type FacultyType = FacultyImport;
export const FacultyValue = FacultyImport;

export type ProfileType = ProfileImport;
export const ProfileValue = ProfileImport;

export type LessonTypeType = LessonTypeImport;
export const LessonTypeValue = LessonTypeImport;

export type LessonType = LessonImport;
export const LessonValue = LessonImport;

export type SemesterType = SemesterImport;
export const SemesterValue = SemesterImport;

export type ScheduleType = ScheduleImport;
export const ScheduleValue = ScheduleImport;

export type SubgroupLessonType = SubgroupLessonImport;
export const SubgroupLessonValue = SubgroupLessonImport;

export type LessonInScheduleType = LessonInScheduleImport;
export const LessonInScheduleValue = LessonInScheduleImport;

export type GroupType = GroupImport;
export const GroupValue = GroupImport;

export type SubgroupType = SubgroupImport;
export const SubgroupValue = SubgroupImport;

export type OverrideDayType = OverrideDayImport;
export const OverrideDayValue = OverrideDayImport;

export type LessonNotInScheduleType = LessonNotInScheduleImport;
export const LessonNotInScheduleValue = LessonNotInScheduleImport;

export type PaginatedInputType = PaginatedInputImport;
export const PaginatedInputValue = PaginatedInputImport;

import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Field, Int, ObjectType} from "type-graphql";

@ObjectType()
@Entity({name: "semesters"})
export class Semester {
    @Field(() => Int)
    @PrimaryGeneratedColumn("increment")
    id?: number;

    @Field(() => Int)
    @Column({ type: "int" })
    year: number;

    @Field(() => Boolean)
    @Column({ type: "boolean" })
    isFirstSemester: boolean;
}

export default Semester;

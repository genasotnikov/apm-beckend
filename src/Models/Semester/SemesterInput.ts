import {Field, InputType, Int} from "type-graphql";

@InputType()
class SemesterInput {

    @Field(() => Int)
    year: number;

    @Field(() => Boolean)
    isFirstSemester: boolean;
}

export default SemesterInput

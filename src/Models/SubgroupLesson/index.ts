import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {LessonInScheduleType, LessonInScheduleValue, SubgroupType, SubgroupValue} from "../../Models";
import {Field, ID, ObjectType} from "type-graphql";

@ObjectType()
@Entity("subgroup_lesson")
class SubgroupLesson {
    @Field(() => ID)
    @PrimaryGeneratedColumn("increment")
    readonly id: number;

    @OneToOne(() => SubgroupValue)
    @Column({name: "subgroup_id", type: "int"})
    private subgroupId: number;

    get getSubgroupId():number {
        return this.subgroupId;
    }

    @Field(type => SubgroupValue)
    private subgroup: SubgroupType;

    get getSubgroup():SubgroupType {
        return this.subgroup
    }

    set setSubgroup(subgroup: SubgroupType) {
        this.subgroup = subgroup;
    }

    @Column({name: "lesson_in_schedule_id", type: "int"})
    private lessonInScheduleId: number;

    get getLessonInScheduleId():number {
        return this.lessonInScheduleId;
    }

    @Field(() => LessonInScheduleValue)
    private lessonInSchedule: LessonInScheduleType;

    get getLessonInSchedule() {
        return this.lessonInSchedule;
    }

    set setLessonInSchedule(lessonInSchedule: LessonInScheduleType) {
        this.lessonInSchedule = lessonInSchedule;
    }

    setValues(subgroupId: number, lessonInScheduleId: number) {
        this.subgroupId = subgroupId;
        this.lessonInScheduleId = lessonInScheduleId;
    }
}

export default SubgroupLesson;

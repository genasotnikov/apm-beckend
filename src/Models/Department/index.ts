import {Field, ObjectType} from "type-graphql";
import {Column, Entity} from "typeorm";

@ObjectType()
@Entity({name: "department"})
class Department {
    @Column({ primary: true })
    @Field()
    readonly name: string;

    @Column()
    @Field({nullable: true})
    readonly director: string;
}

export default Department;
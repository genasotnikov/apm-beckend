import {Field, ID, ObjectType} from "type-graphql";
import {Entity, Column, PrimaryGeneratedColumn} from "typeorm";
import { TeacherType, TeacherValue }  from "../index";

@Entity()
@ObjectType()
class User {
    @Field(type => ID)
    @PrimaryGeneratedColumn("increment")
    id: number;

    @Column({unique: true})
    @Field()
    public email:string;

    @Column({unique: true})
    @Field()
    socialToken?:string;

    @Column()
    password?:string;

    @Field(type => TeacherValue)
    teacher: TeacherType;

    @Column()
    firstName: string;

    @Column({nullable: true})
    middleName: string;

    @Column()
    lastName: string;

    @Column()
    department: number;

    // for registration
    setValue(
        email: string | null = null,
        password: string | null = null,
        teacher: TeacherType,
        socialToken?:string,
        hasSchedule?: boolean
        ) {
        if (password) this.password = password;
        this.teacher = teacher;
        this.firstName = teacher.firstName;
        this.lastName = teacher.lastName;
        this.middleName = teacher.middleName;
        if (email) this.email = email;
        else this.socialToken = socialToken;
    }
}

export default User;

let assert = require("assert");
import SubgroupResolver from "../Subgroup";

describe('Subgroup resolver test', () => {
    const subgroupRes = new SubgroupResolver();
    it("should get subgroups by ids", async () => {
        const subgroups = (await subgroupRes.getSubgroupsByIds({ ids: [10] }))[0];
        assert(subgroups.getNumber === 1 && subgroups.getGroupId === 2, true);
    });
});

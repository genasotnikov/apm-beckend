import Controller from "../../services/Controller";
import {SubgroupType, SubgroupValue} from "../../Models";
import {Arg, Field, InputType, Int, Query} from "type-graphql";
import SubgroupLogic from "../../Logic/Subgroup";

@InputType()
class getSubgroupsArgs {
    @Field(() => Int)
    groupId?: number;
}

@InputType()
class getSubgroupsByIdsArgs {
    @Field(() => [Int])
    ids: number[]
}

class SubgroupResolver {

    @Query(() => [SubgroupValue])
    async getSubgroups(@Arg("getSubgroupsArgs")args: getSubgroupsArgs) {
        const groupsInDb = await this.subgroupController.find(args);
        return groupsInDb.map(group => this.subgroupLogic.getSubgroup(group));
    }

    @Query(() => [SubgroupValue])
    async getSubgroupsByIds(@Arg("getSubgroupsByIdsArgs"){ ids }: getSubgroupsByIdsArgs) {
        const subgroupsInDb = await this.subgroupController.findByIds(ids);
        for (let group of subgroupsInDb) {
            group = await this.subgroupLogic.getSubgroup(group);
        }
        return subgroupsInDb;
    }

    private subgroupLogic = new SubgroupLogic();
    public subgroupController = new Controller<SubgroupType>(SubgroupValue);
}

export default SubgroupResolver;

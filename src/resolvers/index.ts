import UserResolver from "./User";
import LessonTypeResolver from "./LessonType";
import LessonResolver from "./Lesson";
import LessonInScheduleResolver from "./LessonInSchedule";
import SubgroupLessonResolver from "./SubgroupLesson";
import FacultyResolver from "./Faculty";
import VedomostResolver from "./Vedomost";
import OverrideDayResolver from "./OverrideDay";
import SubgroupResolver from "./Subgroup";
import GroupResolver from "./Group";


export default [
    UserResolver,
    LessonTypeResolver,
    LessonResolver,
    LessonInScheduleResolver,
    SubgroupLessonResolver,
    FacultyResolver,
    VedomostResolver,
    OverrideDayResolver,
    SubgroupResolver,
    GroupResolver
];

import Controller from "../../services/Controller";
import {GroupType, GroupValue, PaginatedInputType, PaginatedInputValue} from "../../Models";
import ProfileResolver from "../Profile";
import {Arg, Field, InputType, Int, Mutation, Query} from "type-graphql";
import GroupLogic from "../../Logic/Group";
import GroupInput from "../../Models/Group/GroupInput";
import Group from "../../Models/Group";
import {ApolloError} from "apollo-server-errors";

@InputType()
class getGroupsArgs {
    @Field(() => Int, { nullable: true })
    year?: number;

    @Field(() => Int, { nullable: true })
    profileId?: number;
}

class GroupResolver extends ProfileResolver {

    @Query(() => [GroupValue])
    private async getGroups(@Arg("getGroupsArgs")args: getGroupsArgs) {
        const groupsInDb = await this.groupController.find(args);
        return groupsInDb.map(group => this.groupLogic.getGroup(group));
    }

    @Query(() => [GroupValue])
    private async getPaginatedGroups(@Arg("paginatedInput", () => PaginatedInputValue)paginatedInput: PaginatedInputType) {
        return await this.groupLogic.getPaginatedData(paginatedInput);
    }

    @Query(() => Int)
    private async getGroupsCount() {
        return await this.groupLogic.getCount();
    }

    @Mutation(() => String)
    private async deleteGroup(@Arg("id", () => Int) id: number) {
        try {
            await this.groupLogic.delete(id);
            return "Success";
        } catch (e) {
            throw new ApolloError(e);
        }
    }

    @Mutation(() => String)
    private async createGroup(@Arg("newGroup", () => GroupInput)newGroup: GroupInput) {
        try {
            await this.groupLogic.create(newGroup as unknown as Group);
            return "Success";
        } catch (e) {
            throw new ApolloError(e);
        }

    }

    private groupLogic = new GroupLogic();
    protected groupController = new Controller<GroupType>(GroupValue);
}

export default GroupResolver;

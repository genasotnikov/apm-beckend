import Controller from "../../services/Controller";
import {ProfileType, ProfileValue} from "../../Models";
import FacultyResolver from "../Faculty";
import {Arg, Field, InputType, Int, Query} from "type-graphql";

@InputType()
class getProfilesOfFacultyAgrs {

    @Field(() => Int)
    facultyId: number
}

class ProfileResolver extends FacultyResolver {

    @Query(() => [ProfileValue])
    private async getProfilesOfFaculty(@Arg("getProfilesOfFacultyAgrs"){facultyId}: getProfilesOfFacultyAgrs) {
        return await this.profileController.find({facultyId});
    }

    protected profileController = new Controller<ProfileType>(ProfileValue);
}

export default ProfileResolver;

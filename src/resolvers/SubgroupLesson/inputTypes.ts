import {Field, InputType, Int} from "type-graphql";
import {AddLessonInScheduleTypeArgs} from "../LessonInSchedule";

@InputType()
export class AddSubgroupLessonTypeArgs extends AddLessonInScheduleTypeArgs{
    @Field(() => Int)
    readonly lessonId: number;

    @Field(() => Int)
    readonly subgroupId: number;
}

@InputType()
export class AddArraySubgroupLessonTypeArgs {
    @Field(() => [AddSubgroupLessonTypeArgs])
    lessons: AddSubgroupLessonTypeArgs[]
}

@InputType()
export class AddSubgroupLessonsTypeArgs {
    @Field(() => AddSubgroupLessonTypeArgs)
    arrayOfLessons: AddSubgroupLessonTypeArgs[]
}

@InputType()
export class GetDayScheduleArgs {
    @Field(() => Int)
    semesterId: number;

    @Field(() => Int)
    weekIndex: number;

}

@InputType()
export class GetSubgroupLessonsArgs {
    @Field(() => GetDayScheduleArgs)
    arg: GetDayScheduleArgs
}

import {Arg, Mutation, Query, Resolver, Int} from "type-graphql";
import {SubgroupLessonType, SubgroupLessonValue} from "../../Models";
import {CurrentUser} from "../../customDecorators";
import {ApolloError} from "apollo-server-errors";
import {
    AddSubgroupLessonTypeArgs,
    AddSubgroupLessonsTypeArgs,
    AddArraySubgroupLessonTypeArgs,
    GetSubgroupLessonsArgs
} from "./inputTypes";
import LessonInScheduleLogic from "../../Logic/LessonInSchedule";
import ScheduleLogic from "../../Logic/Schedule";
import SubgroupLessonLogic from "../../Logic/SubgroupLesson";


@Resolver()
class SubgroupLessonResolver {

    @Mutation(() => SubgroupLessonValue)
    public async addSubgroupLesson(
        @Arg("AddSubgroupLessonTypeArgs")
            args : AddSubgroupLessonTypeArgs,
        @CurrentUser() userId : number) {
        return await this.subgroupLessonLogic.addSubgroupLessonToDb(args, userId);
    }

    @Mutation(() => String)
    public async addArraySubgroupLesson(
        @Arg("AddArraySubgroupLessonTypeArgs")
            { lessons } : AddArraySubgroupLessonTypeArgs,
        @CurrentUser() userId : number) {
        try {
            for (const lesson of lessons) {
                try {
                    await this.subgroupLessonLogic.addSubgroupLessonToDb(lesson, userId);
                } catch (e) {
                    throw new ApolloError(e);
                }
            }
            return "Success";
        } catch (e) {
            throw new ApolloError(e);
        }

    }

    @Query(() => [SubgroupLessonValue])
    public async getSubgroupLessons(@Arg("GetSubgroupLessonsArgs")args : GetSubgroupLessonsArgs, @CurrentUser() userId : number) {
        let res = new Array<SubgroupLessonType>();
        const scheduleId = await this.scheduleLogic.getScheduleIdByTeacherAndSchedule(userId, args.arg.semesterId);
        if (scheduleId) {
            const lessonsInSchedule = await this.lessonInScheduleLogic.getDayLessonsInSchedule(scheduleId, args.arg.weekIndex);
            res = await this.subgroupLessonLogic.getSubgroupLessonsByLessonInScheduleIds(lessonsInSchedule.map(lis => lis.id));
        }
        return res;
    };

    @Mutation(() => String)
    public async addSubgroupLessons(@Arg("AddSubgroupLessonsTypeArgs") { arrayOfLessons } : AddSubgroupLessonsTypeArgs,
                @CurrentUser() userId : number) {
        let lesson;
        for (lesson of arrayOfLessons) {
            try {
                await this.subgroupLessonLogic.insertIntoDb({
                    lessonInScheduleId: lesson.lessonId,
                    subgroupId: lesson.subgroupId
                });
            } catch (e) {
                throw new ApolloError("Internal Server Error","500");
            }
        }
        return "Success";
    }

    @Mutation(() => String)
    public async deleteSubgroupLessonsByLessonImScheduleId(
        @Arg("LessonInScheduleId", () => Int )lisId: number,
        @CurrentUser() userId : number) {
        try {
            await this.subgroupLessonLogic.deleteUserLessonInSchedule(lisId);
            return "Success";
        } catch (e) {
            throw new ApolloError(e);
        }
    }

    private lessonInScheduleLogic = new LessonInScheduleLogic();
    private scheduleLogic = new ScheduleLogic();
    private subgroupLessonLogic = new SubgroupLessonLogic();
}

export default SubgroupLessonResolver;

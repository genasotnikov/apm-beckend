// @ts-ignore
import { reservedSubgroupLessonsData as dataForSend } from "../../Logic/utils.spec";

const assert = require("assert");
const SubgroupLessonResolver = require("./index").default;
// @ts-ignore
const SubgroupLessonLogic = require("../../Logic/SubgroupLesson/index").default;
const LessonInScheduleLogic = require("../../Logic/LessonInSchedule").default;



describe('Subgroup lesson resolver test', () => {
    const subgroupLessonResolver = new SubgroupLessonResolver();
    const lessonInScheduleLogic = new LessonInScheduleLogic();
    const subgroupLessonLogic = new SubgroupLessonLogic();

    it('should add array of lessons and find them in db', async function () {
        let res = true;
        await subgroupLessonResolver.addArraySubgroupLesson({...dataForSend}, 1);
        for (let oneLesson of dataForSend.lessons) {
            const subgroupId = oneLesson.subgroupId;
            delete oneLesson.subgroupId;
            const lis = (await lessonInScheduleLogic.findLessonsInSchedule(oneLesson))[0];
            if (lis) {
                const createdSubgroupLesson =
                    (await subgroupLessonLogic.findSubgroupLessons({ subgroupId: subgroupId, lessonInScheduleId: lis.id }))[0];
                if (!createdSubgroupLesson) {
                    res = false;
                    break;
                }
            } else {
                res = false;
                break;
            }
        }
        assert.equal(res, true);
    });
});

import Controller from "../../services/Controller";
import {FacultyType, FacultyValue, GroupType, SubgroupType, SubgroupValue} from "../../Models";
import {Query, Resolver} from "type-graphql";
import {ApolloError} from "apollo-server-errors";

@Resolver()
class FacultyResolver {

    @Query(() => [FacultyValue])
    protected async getFaculties(): Promise<FacultyType[]> {
        try {
            return await this.facultyController.find();
        } catch (e) {
            throw new ApolloError("Internal", "500");
        }
    }

    protected facultyController = new Controller<FacultyType>(FacultyValue);
}

export default FacultyResolver;

import {Arg, Mutation, Query, Resolver} from "type-graphql";
import {AuthenticationError, UserInputError, ApolloError } from "apollo-server-errors";
import jwt from "jsonwebtoken";
import LoginArgs from "./loginArgs";
import ReginsterEmailArgs from "./RegisterEmailArgs";
import ScheduleLogic from "../../Logic/Schedule";
import {UserValue, TeacherValue, UserType} from "../../Models";
import Controller from "../../services/Controller";
import {CurrentUser} from "../../customDecorators";
import Department from "../../Models/Department";

@Resolver(UserValue)
class UserResolver {

    async findUserInDB(email:string, password:string): Promise<string|null> {
        const user = await this.controller.find({ email, password });
        if (user.length > 0) return jwt.sign(email,"TypeGraphQL", );
        else throw new AuthenticationError("Wrong email or password");
    }

    async addUser(user: UserType) {
        const registeredUser = await this.controller.find({email: user.email});
        if (registeredUser.length) throw new UserInputError("User is already exists");
        await this.controller.add(user);
        return this.findUserInDB(user.email, user.password);
    }

    @Query(returns => UserValue)
    async getUserData(@CurrentUser() userId : number) {
        try {
            const user = await this.controller.findByIds([userId]);
            if (user.length > 0) {
                const schedule = (await this.scheduleLogic.getCurrentSchedule(userId));
                const department = (await this.departmentController.find({ name: user[0].department }))[0];
                const teacher = new TeacherValue(
                    user[0].firstName,
                    user[0].middleName,
                    user[0].lastName,
                    schedule.isFinished || false,
                    department);
                const User = new UserValue();
                User.setValue(user[0].email, null, teacher);
                return User;
            }
            else throw new AuthenticationError("Wrong token");
        } catch (e) {
            throw new ApolloError(e, "500");
        }
    }

    @Query(returns => String)
    async login(@Arg("LoginArgs") { email, password } : LoginArgs ) {
        return await this.findUserInDB(email, password);
    }

    @Mutation(() => String)
    async registerThroughEmail(@Arg("ReginsterEmailArgs") {
        email, password, firstName, lastName, middleName } : ReginsterEmailArgs) {
        const newTeacher = new TeacherValue(firstName, middleName, lastName);
        const newUser = new UserValue();
        newUser.setValue(email, password, newTeacher);
        return this.addUser(newUser);
    }

    private scheduleLogic = new ScheduleLogic();
    private departmentController = new Controller<Department>(Department);
    private controller = new Controller<UserType>(UserValue);
}

export default UserResolver;

import {Field, InputType} from "type-graphql";

@InputType()
class ReginsterEmailArgs {
    @Field()
    email: string;

    @Field()
    password: string;

    @Field()
    firstName: string;

    @Field()
    lastName: string;

    @Field({nullable: true})
    middleName: string;
}

export default ReginsterEmailArgs;
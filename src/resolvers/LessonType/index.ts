import {Arg, Field, Float, InputType, Mutation, Query, Resolver} from "type-graphql";
import { ForbiddenError } from "apollo-server-errors";
import { LessonTypeValue, LessonTypeType } from "../../Models";
import Controller from "../../services/Controller";
import {CurrentUser} from "../../customDecorators";
import LessonType from "../../Models/LessonType";

@InputType()
class AddLessonTypeArgs {
    @Field()
    name: string

    @Field(returns => Float)
    time: number
}

@InputType()
class DeleteLessonTypeArgs {
    @Field(type => [String])
    ids: number[]
}

@InputType()
class UpdateLessonTypeArgs {
    @Field()
    id: number;
    @Field()
    name: string;
    @Field(type => Float)
    time: number;
}

@Resolver(LessonTypeValue)
class LessonTypeResolver {

    @Mutation(() => LessonType)
    async addLessonType( @Arg("AddLessonTypeArgs") { name, time } : AddLessonTypeArgs, @CurrentUser() userId : number ) {
        const NewLessonType = new LessonTypeValue();
        NewLessonType.setValue(name, userId, time);
        await this.controller.add(NewLessonType);
        return (await this.controller.find(NewLessonType))[0];
    }

    @Mutation(() => String)
    async updateLessonType( @Arg("UpdateLessonTypeArgs") { id, name, time } : UpdateLessonTypeArgs, @CurrentUser() userId : number ) {
        if (this.constantIDs.has(id)) throw new ForbiddenError("You don't have permission");
        const NewLessonType = new LessonTypeValue();
        NewLessonType.setValue(name, userId, time);
        await this.controller.update(id, NewLessonType);
        return "Success";
    }

    @Mutation(() => String)
    async deleteLessonType( @Arg("DeleteLessonTypeArgs") { ids } : DeleteLessonTypeArgs, @CurrentUser() userId : number ) {
        ids.every(id => {
            if (this.constantIDs.has(id)) throw new ForbiddenError("You don't have permission");
        });
        await this.controller.deleteByIds(ids);
        return "Success";
    }

    @Query(() => [LessonType])
    async lessonTypes(@CurrentUser() userId : number ) {
        const constantValues:LessonType[] = await this.controller.findByIds([...this.constantIDs]) as LessonType[];
        const userValues: LessonType[] = await this.controller.find({teacherId: userId}) as LessonType[];
        return [...constantValues, ...userValues].sort((a:LessonType,b:LessonType) => a.id-b.id);
    }

    private constantIDs = new Set<number>([1,2,3,4,5,6,7]);
    private controller = new Controller<LessonTypeType>(LessonTypeValue);
}

export default LessonTypeResolver;

import Controller from "../../services/Controller";
import { ApolloError } from "apollo-server-errors";
import {LessonType, LessonTypeValue, LessonValue, OverrideDayType, OverrideDayValue} from "../../Models";
import {Arg, Mutation, Resolver} from "type-graphql";
import {CurrentUser} from "../../customDecorators";
import AddOverrideDayArgs from "./AddOverrideDayArgs";

@Resolver()
class OverrideDayResolver {
    @Mutation(() => OverrideDayValue)
    async addOverrideDay( @Arg("addOverrideDayArgs") { date } : AddOverrideDayArgs,
                          @CurrentUser() userId : number ) {
        const NewOverrideDay = new OverrideDayValue();
        const foundOverrideDay = (await this.overrideDayController.find({ teacherId: userId, date }))[0];
        if (foundOverrideDay) return foundOverrideDay;
        NewOverrideDay.setValue(date, userId);
        await this.overrideDayController.add(NewOverrideDay);
        return (await this.overrideDayController.find(NewOverrideDay))[0];
    }

    protected overrideDayController = new Controller<OverrideDayType>(OverrideDayValue);
}

export default OverrideDayResolver;

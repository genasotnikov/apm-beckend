import {Field, InputType} from "type-graphql";

@InputType()
class AddOverrideDayArgs {
    @Field(() => Date)
    date: Date;
}

export default AddOverrideDayArgs;

import {Arg, Field, InputType, Int, Mutation, Query, Resolver} from "type-graphql";
import {ApolloError} from "apollo-server-errors";
import { LessonType, LessonValue, LessonTypeValue, LessonTypeType } from "../../Models";
import Controller from "../../services/Controller";
import {CurrentUser} from "../../customDecorators";


@InputType()
class AddLessonsArgs {
    @Field(type => String)
    content: string;

    @Field(type => Int)
    typeId: number;
}


@Resolver()
class LessonResolver {

    @Mutation(() => LessonValue)
    async addLesson( @Arg("AddLessonsArgs") { content, typeId } : AddLessonsArgs, @CurrentUser() userId : number ) {
        const NewLesson = new LessonValue();
        NewLesson.setValue(userId, content, typeId);
        try {
            await this.controller.add(NewLesson);
            const res = (await this.controller.find(NewLesson))[0];
            res.setLessonType((await this.lessonTypeController.findByIds([res.typeId]))[0])
            return res;
        } catch (e) {
            throw new ApolloError("Internal Server Error", "500");
        }
    }

    @Query(() => [LessonValue])
    async lessons(@CurrentUser() userId : number ) {
        const userLessons: LessonType[] = await this.controller.find({teacherId: userId}) as LessonType[];
        const lessonTypeIds: number[] = userLessons.map(lesson => lesson.typeId);
        const lessonTypes = await this.lessonTypeController.findByIds(lessonTypeIds);
        userLessons.map(lesson => {
            const lessonType = lessonTypes.filter(type => type.id === lesson.typeId)[0];
            lesson.setLessonType(lessonType);
            return lesson;
        });
        return userLessons;
    }

    private lessonTypeController = new Controller<LessonTypeType>(LessonTypeValue);
    private controller = new Controller<LessonType>(LessonValue);
}

export default LessonResolver;

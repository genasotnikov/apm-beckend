import Controller from "../../services/Controller";
import {LessonNotInScheduleType, LessonNotInScheduleValue} from "../../Models";
import {Resolver} from "type-graphql";
import OverrideDayResolver from "../OverrideDay";

@Resolver()
class LessonNotInScheduleResolver {



    overrideDayResolver = new OverrideDayResolver();
    controller = new Controller<LessonNotInScheduleType>(LessonNotInScheduleValue)
}

export default LessonNotInScheduleResolver;

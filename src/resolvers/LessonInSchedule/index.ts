import { Field, InputType, Int, Query, Resolver} from "type-graphql";
import Controller from "../../services/Controller";
import {
    LessonInScheduleType,
    LessonInScheduleValue,
    LessonType,
    LessonValue,
} from "../../Models";
import {CurrentUser} from "../../customDecorators";
import LessonInScheduleLogic from "../../Logic/LessonInSchedule";

@InputType()
export class AddLessonInScheduleTypeArgs {
    @Field(() => Int)
    readonly dayIndex: number;

    @Field(() => Int)
    public lessonId: number;

    @Field(() => Int)
    readonly weekIndex: number;

    @Field(() => Boolean)
    readonly isChisl: boolean;
}

@Resolver()
class LessonInScheduleResolver {

    @Query(() => [LessonInScheduleValue])
    private async getAllLessonInSchedules(@CurrentUser() userId : number): Promise<Promise<LessonInScheduleType>[]> {
        const lessonsInScheduleInDb = await this.lessonController.find({userId});
        const lisIds = lessonsInScheduleInDb.map(lis => lis.id);
        this.lessonInScheduleController.repName = "lessons_in_schedule";
        const teacherLis:LessonInScheduleType[] = await this.lessonInScheduleController.findByFKIds("lessonId", lisIds);
        return teacherLis.map(async lis => {
            return await this.lessonInScheduleLogic.getLessonInSchedule(lis)
        });
    }

    protected lessonController = new Controller<LessonType>(LessonValue);
    protected lessonInScheduleController = new Controller<LessonInScheduleType>(LessonInScheduleValue);
    private lessonInScheduleLogic = new LessonInScheduleLogic();
}

export default LessonInScheduleResolver;

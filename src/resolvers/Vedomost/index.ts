import {Arg, Field, InputType, Int, Query, Resolver} from "type-graphql";
import { SpreadsheetVedomost } from "vedomost-spreadsheet";
import { CurrentUser} from "../../customDecorators";
import Controller from "../../services/Controller";
import {
    OverrideDayType,
    OverrideDayValue,
} from "../../Models";
import {dayOverrideScheduleByDayId, dayScheduleByWeekIndex} from "../../../queries";
import FullDayInfoOfVedomost from "../../Models/Vedomost/FullLessonInfoOfVedomost";
import SubgroupLessonOfVedomost from "../../Models/Vedomost/SubgroupLessonOfVedomost";
import FullLessonInfo from "../../Models/Vedomost/FullLessonInfo";
import SemesterLogic from "../../Logic/Semester";
import ScheduleLogic from "../../Logic/Schedule";
import DateInProgram from "../../Logic/DateInProgram";
import {ApolloError} from "apollo-server-errors";



@InputType()
class getVedomostDaysArgs {
    @Field(() => Int)
    monthIndex: number;
    @Field(() => Int)
    year: number;
}

@Resolver()
class VedomostResolver {
    @Query(() => [FullDayInfoOfVedomost])
    private async getVedomostDays(
        @Arg("getVedomostDaysArgs") { monthIndex, year } : getVedomostDaysArgs,
        @CurrentUser() userId : number) {
        const res = new Array<FullDayInfoOfVedomost>();
        const semesterId = await this.semesterLogic.getSemesterIdByYearAndMonth(year, monthIndex);

        if (!semesterId) throw new ApolloError("Нет данных о семестре!");

        const scheduleId = await this.scheduleLogic.getScheduleIdByTeacherAndSchedule(userId, semesterId);

        if (!scheduleId) throw new ApolloError("У преподавателя нет расписания на семестр!");
        let isChisl = (new DateInProgram(year, monthIndex, 1)).isChisl();
        await DateInProgram.iterateDaysInMonthAsync(year, monthIndex, async iterableDate => {
            const dayOfWeek = iterableDate.getDay();
            if (!iterableDate.isSunday()) {
                const overrideDay = (await this.overrideDayController.find({ _date: iterableDate, _teacherId: userId }))[0];
                const subgrLessons = await this.constroller.query(overrideDay
                    ? dayOverrideScheduleByDayId(overrideDay.id)
                    : dayScheduleByWeekIndex(dayOfWeek - 1, scheduleId, isChisl));
                const dayLessonsFullInfo = new Array<FullLessonInfo>();
                subgrLessons.forEach((subgrLesson: SubgroupLessonOfVedomost) => {
                    const existingElement = dayLessonsFullInfo.find(
                        (el: FullLessonInfo) => subgrLesson.dayIndex === el.getDayIndex);
                    if (existingElement && (existingElement.getDayIndex !== undefined)) {
                        existingElement.addSubgroup(subgrLesson.groupSubgroup);
                    }
                    else {
                        const newEl = new FullLessonInfo();
                        newEl.setValue(subgrLesson);
                        dayLessonsFullInfo.push(newEl);
                    }
                });
                dayLessonsFullInfo.sort((prev,next) => { if (prev.getDayIndex < next.getDayIndex) return -1});
                const dayData = new FullDayInfoOfVedomost();
                dayData.setValue(iterableDate, dayLessonsFullInfo);
                if (dayLessonsFullInfo.length) res.push(dayData);
            } else {
                isChisl = !isChisl;
            }
        });

        return res;
    }

    @Query(() => String)
    async uploadVedomost(
        @Arg("getVedomostDaysArgs") { monthIndex, year } : getVedomostDaysArgs,
        @CurrentUser() userId : number) {
        try {
            // const vedomostDays = await this.getVedomostDays({ monthIndex, year }, userId); TODO получить данные и сделать настоящую ведомость
            return await (new SpreadsheetVedomost()).getFileBuffer();
        } catch (e) {
            throw new ApolloError(e);
        }
    }

    scheduleLogic = new ScheduleLogic();
    semesterLogic = new SemesterLogic();
    overrideDayController = new Controller<OverrideDayType>(OverrideDayValue);
    constroller = new Controller<SubgroupLessonOfVedomost>(null);
}

export default VedomostResolver;

import {ApolloError} from "apollo-server-errors";
import {Arg, Int, Mutation, Query, Resolver} from "type-graphql";
import Controller from "../../services/Controller";
import {
    PaginatedInputType,
    PaginatedInputValue,
    SemesterType,
    SemesterValue,
} from "../../Models";
import SemesterLogic from "../../Logic/Semester";
import SemesterInput from "../../Models/Semester/SemesterInput";

const firstSemester = [8, 9, 10, 11, 1, 0];

@Resolver()
class SemesterResolver {

    @Query(() => SemesterValue)
    async getCurrentSemester() {
        const currentDate = new Date();
        try {
            return await this.semesterLogic.findSemester({
                year: currentDate.getFullYear(),
                isFirstSemester: firstSemester.some(val => val === currentDate.getMonth())
            });
        } catch (e) {
            throw new ApolloError(e);
        }
    }

    @Query(() => [SemesterValue])
    async getPaginatedSemester(@Arg("paginatedInput", () => PaginatedInputValue)paginatedInput: PaginatedInputType) {
        return await this.semesterLogic.getPaginatedData(paginatedInput);
    }

    @Query(() => Int)
    async getCountOfSemester() {
        return await this.semesterLogic.getCount();
    }

    @Mutation(() => String)
    async createSemester(@Arg("SemesterInput") semesterInput: SemesterInput) {
        try {
            await this.semesterLogic.create(semesterInput);
            return "Success";
        } catch (e) {
            throw new ApolloError(e);
        }
    }

    @Mutation(() => String)
    async deleteSemester(@Arg("id", () => Int) id: number) {
        try {
            await this.semesterLogic.delete(id);
            return "Success";
        } catch (e) {
            throw new ApolloError(e);
        }
    }

    private semesterLogic = new SemesterLogic();
    protected semesterController = new Controller<SemesterType>(SemesterValue);
    protected findSemester = this.semesterLogic.findSemester;
}

export default SemesterResolver;

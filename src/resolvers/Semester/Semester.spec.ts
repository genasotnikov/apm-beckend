let assert = require("assert");
import SemesterResolver from "../Semester";

describe('Semester resolver test', () => {
    const semesterResolver = new SemesterResolver();
    it("should get current semester", async () => {
        const currentDate = new Date();
        const currentMonth = currentDate.getMonth();
        const firstSemesterMonths = [8,9,10,11,0];
        const currentSemester = await semesterResolver.getCurrentSemester();
        const isFirstSemester = firstSemesterMonths.some((el) => currentMonth === el);
        assert(currentSemester.year === currentDate.getFullYear()
            && ((isFirstSemester && currentSemester.isFirstSemester)
                || !(isFirstSemester || currentSemester.isFirstSemester)));
});
});

import { Request } from "express";
import jwt from "jsonwebtoken";
import {AuthenticationError} from "apollo-server-errors";
import { createParamDecorator } from "type-graphql";
import Controller from "./services/Controller";
import {UserType, UserValue} from "./Models";

export const CurrentUser = () => {
    return createParamDecorator<any>(async ({ context }) => {
        const req: Request = context.req;
        const token: string = req.get('Authorization');
        const userEmail = jwt.decode(token);
        const UserController = new Controller<UserType>(UserValue);
        const res: UserType[] = await UserController.find({email: userEmail}) as UserType[];
        if (res.length) {
            return res[0].id;
        }
        throw new AuthenticationError("Your key is wrong");
    });
};

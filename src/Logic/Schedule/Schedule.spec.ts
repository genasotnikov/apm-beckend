// @ts-ignore
let assert = require("assert");
const ScheduleLogic = require("./index").default;

describe('Schedule logic test', () => {
    const scheduleLogic = new ScheduleLogic();
    it('should return 1', async function () {
        const res = await scheduleLogic.getScheduleIdByTeacherAndSchedule(1, 2);
        assert.equal(res, 1);
    });
});

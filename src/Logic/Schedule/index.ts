import {ScheduleType, ScheduleValue} from "../../Models";
import {ApolloError} from "apollo-server-errors";
import SemesterLogic from "../Semester";
import CRUD from "../CRUD";

class ScheduleLogic extends CRUD<ScheduleType> {

    constructor() {
        super(ScheduleValue);
    }

    // TODO test
    protected async createCurrentSchedule(userId: number, semesterId: number) {
        try {
            const schedule = new ScheduleValue();
            schedule.setValue(semesterId, userId, false);
            await this.controller.add(schedule);
            return (await this.controller.find(schedule))[0];
        } catch (e) {
            throw new ApolloError("Internal", "500");
        }
    }

    // TODO test
    async getCurrentSchedule(userId: number) {
        try {
            const currentDate = new Date();
            const currentSemesterId = (await this.semesterLogic.findSemester({
                isFirstSemester: (currentDate.getMonth() > 7 || currentDate.getMonth() === 0),
                year: currentDate.getFullYear()
            })).id;
            const res = (await this.controller.find({ semesterId: currentSemesterId }))[0];
            if (!res) {
                return await this.createCurrentSchedule(userId, currentSemesterId);
            }
            return res;
        } catch (e) {
            throw new ApolloError(e, "500");
        }
    }

    // TODO test
    getScheduleById = async (id: number) => {
        try {
            const schedule = (await this.controller.findByIds([id]))[0];
            return schedule
        } catch (e) {
            throw new ApolloError(e, "500")
        }
    };

    getScheduleIdByTeacherAndSchedule = async (userId: number, semesterId: number) => {
        const schedule = (await this.controller.find({ teacherId: userId, semesterId } as ScheduleType))[0]

        return schedule && schedule.id;
    };

    private semesterLogic = new SemesterLogic();
}

export default ScheduleLogic;

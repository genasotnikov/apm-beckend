import CRUD from "../CRUD";
import {ProfileType, ProfileValue} from "../../Models";
import FacultyLogic from "../Faculty";

class ProfileLogic extends CRUD<ProfileType> {
    constructor() {
        super(ProfileValue);
    }

    async getProfile(profile: ProfileType): Promise<ProfileType> {
        profile.setFaculty = await this.facultyLogic.getFaculty(profile.facultyId);
        return profile;
    }

    private facultyLogic = new FacultyLogic();
}

export default ProfileLogic;

import CRUD from "../CRUD";
import {GroupType, GroupValue, ProfileType, ProfileValue} from "../../Models";
import ProfileLogic from "../Profile";
import Controller from "../../services/Controller";

class GroupLogic extends CRUD<GroupType> {
    constructor() {
        super(GroupValue);
    }

    async getGroup(group: GroupType): Promise<GroupType> {
        const profile: ProfileType = (await this.profileController.findByIds([group.getProfileId]))[0];
        group.setProfile = await this.profileLogic.getProfile(profile);
        return group;
    }

    private profileController = new Controller<ProfileType>(ProfileValue);
    private profileLogic = new ProfileLogic();
}

export default GroupLogic;

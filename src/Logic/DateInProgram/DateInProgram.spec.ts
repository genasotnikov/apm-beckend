// @ts-ignore
let assert = require("assert");
const DateInProgram = require("./index").default;

describe('Date in program test', () => {
    it('should return 31', async function () {
        let i = 0;
        await DateInProgram.iterateDaysInMonthAsync(2020,0, () => { i += 1 });
        assert.equal(i, 31 );
    });

    it('should return 29', async function () {
        let i = 0;
        await DateInProgram.iterateDaysInMonthAsync(2020,1, () => { i += 1 });
        assert.equal(i, 29 );
    });
});

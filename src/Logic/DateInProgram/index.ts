class DateInProgram extends Date {

    isSunday:() => boolean = () => {
        return this.getDay() === 0;
    };

    addDay: () => DateInProgram = () => {
        return new DateInProgram(this.getFullYear(), this.getMonth(), this.getDay()+1);
    };

    isChisl: () => boolean = () => {
        // TODO пока будет работать для первого семестра
        const firstOfSeptember = new DateInProgram((new Date()).getFullYear(), 8, 1);
        const firstDayOfSemester = firstOfSeptember.isSunday() ? firstOfSeptember.addDay() : firstOfSeptember;
        const numberOfFirstWeek = firstDayOfSemester.getNumberOfWeek(); // эта неделя числитель
        return (numberOfFirstWeek % 2) === (this.getNumberOfWeek() % 2);
    };

    getNumberOfWeek: () => number = () => {
        const firstDayOfYear = new Date(this.getFullYear(), 0, 1);
        const pastDaysOfYear = (this.valueOf() - firstDayOfYear.valueOf()) / 86400000;
        return Math.ceil((pastDaysOfYear + firstDayOfYear.getDay() + 1) / 7);
    };

    static iterateDaysInMonthAsync = async (year:number, monthIndex: number, callback: (arg0: DateInProgram) => Promise<void>): Promise<void> => {
        for (
            let i=1;
            new Date(year, monthIndex, i).getMonth() === monthIndex;
            i++) {
            const iterableDate = new DateInProgram(year, monthIndex, i);
            await callback(iterableDate);
        }
    }

}

export default DateInProgram;

import Controller from "../../services/Controller";
import {PaginatedInputType} from "../../Models";

interface CRUDInterface<T> {
    getPaginatedData: (paginatedInput: PaginatedInputType) => Promise<T[]>;
    getCount: () => Promise<number>;
    controller: Controller<T>;
}

class CRUD<Table> implements CRUDInterface<Table>{

    protected constructor(tableClass: unknown, repName?: string) {
        this.controller = new Controller<Table>(tableClass);
        this.controller.repName = repName;
    }

    async getPaginatedData(paginatedInput: PaginatedInputType): Promise<Table[]> {
        return await this.controller.getPaginatedData(paginatedInput)
    }

    async getCount(): Promise<number> {
        return await this.controller.getCount();
    }

    async update(id: number, newValue: Table) {
        await this.controller.update(id, newValue);
    }

    async delete(id: number) {
        await this.controller.deleteByIds([id]);
    }

    async create(value: Table) {
        await this.controller.add(value);
    }

    controller: Controller<Table>;
}

export default CRUD;

// @ts-ignore
let assert = require("assert");
import SemesterLogic from "../Semester";

describe('CRUD test', () => {
    const instanceCRUD = new SemesterLogic();
    let newRowId: number;

    it("should get count of rows of semesters table", async () => {
        const count = await instanceCRUD.getCount();
        assert(typeof count, "number");
    });

    it("should create new row", async () => {
        await instanceCRUD.create({ isFirstSemester: true, year: 2090 });
        const createdSemester = await instanceCRUD.findSemester({isFirstSemester: true, year: 2090});
        newRowId = createdSemester?.id;
        assert(createdSemester.year === 2090 && createdSemester.isFirstSemester === true, true);
    });

    it("should delete created row", async () => {
        await instanceCRUD.delete(newRowId);
        const createdSemester = await instanceCRUD.findSemester({isFirstSemester: true, year: 2090});
        assert(!createdSemester, true);
    });
});

import CRUD from "../CRUD";
import {FacultyType, FacultyValue} from "../../Models";

class FacultyLogic extends CRUD<FacultyType> {
    constructor() {
        super(FacultyValue);
    }

    async getFaculty(id: number) {
        return (await this.controller.find({id}))[0];
    }
}

export default FacultyLogic;


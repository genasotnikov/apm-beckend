import CRUD from "../CRUD";
import {GroupType, GroupValue, SubgroupType, SubgroupValue} from "../../Models";
import Controller from "../../services/Controller";
import GroupLogic from "../Group";

class SubgroupLogic extends CRUD<SubgroupType> {
    constructor() {
        super(SubgroupValue);
    }

    public async getSubgroup(subgroup: SubgroupType): Promise<SubgroupType> {
        const group: GroupType = (await this.groupController.findByIds([subgroup.getGroupId]))[0];
        subgroup.setGroup = await this.groupLogic.getGroup(group);
        return subgroup;
    }

    private groupLogic = new GroupLogic();
    private groupController = new Controller<GroupType>(GroupValue);
}


export default SubgroupLogic;

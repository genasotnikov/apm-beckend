import Controller from "../../services/Controller";
import {SubgroupLessonType, SubgroupLessonValue, SubgroupType, SubgroupValue} from "../../Models";
import LessonInScheduleLogic from "../LessonInSchedule";
import {ApolloError} from "apollo-server-errors";
import SubgroupLogic from "../Subgroup";
import {NewSubgroupLesson} from "./types";
import CRUD from "../CRUD";
import {AddSubgroupLessonTypeArgs} from "../../resolvers/SubgroupLesson/inputTypes";

class SubgroupLessonLogic extends CRUD<SubgroupLessonType> {
    constructor() {
        super(SubgroupLessonValue, "SubgroupLesson");
    }

    async addSubgroupLessonToDb({ lessonId, isChisl, subgroupId, weekIndex, dayIndex } : AddSubgroupLessonTypeArgs, userId : number) {
        try {
            const lessonInSchedule = await this.lessonInScheduleLogic.addLessonInSchedule({ lessonId, isChisl, weekIndex, dayIndex }, userId);
            const resInDb = await this.insertIntoDb({ lessonInScheduleId: lessonInSchedule.id, subgroupId });
            const subgroupLesson = await this.getSubgroupLesson(resInDb);
            return subgroupLesson;
        } catch (e) {
            throw new ApolloError(e);
        }
    }

    async getSubgroupLesson(sgl: SubgroupLessonType): Promise<SubgroupLessonType> {
        const lessonInSchedule = (await this.lessonInScheduleLogic.findLessonsInSchedule({id: sgl.getLessonInScheduleId}))[0];
        sgl.setLessonInSchedule = await this.lessonInScheduleLogic.getLessonInSchedule(lessonInSchedule);
        const subgroupInDb = (await this.subgroupController.findByIds([sgl.getSubgroupId]))[0];
        sgl.setSubgroup = await this.subgroupLogic.getSubgroup(subgroupInDb);
        return sgl;
    }

    async getSubgroupLessonsByLessonInScheduleIds(lessonsInScheduleIds: number[]): Promise<SubgroupLessonType[]> {
        let res = new Array<SubgroupLessonType>();
        let subgroupLessons: SubgroupLessonType[];
        try {
            subgroupLessons = await this.controller.findByFKIds("lesson_in_schedule_id", lessonsInScheduleIds);
            for (let sgl of subgroupLessons) {
                sgl = await this.getSubgroupLesson(sgl);
            }
            res = [...subgroupLessons];
        } catch (e) {
            throw new Error(e);
        }
        return res;
    }

    // TODO test
    deleteUserLessonInSchedule = async (lessonInScheduleId: number) => {
        try {
            const foundSubgroupLessons = await this.controller.findByFKIds("lesson_in_schedule_id", [lessonInScheduleId]);
            const foundedIds = foundSubgroupLessons.map(sgl => sgl.id);
            await this.controller.deleteByIds(foundedIds);
            await this.lessonInScheduleLogic.deleteLessonInScheduleById(lessonInScheduleId);
        } catch (e) {
            throw new ApolloError(e);
        }

    };

    async findSubgroupLessons(arg: Object): Promise<SubgroupLessonType[]> {
        try {
            const foundSGLessons = await this.controller.find(arg);
            for (let sgl of foundSGLessons) {
                sgl = await this.getSubgroupLesson(sgl);
            }
            return foundSGLessons;
        } catch (e) {
            throw new ApolloError(e,"500");
        }
    };

    //TODO test
    insertIntoDb = async  ({ lessonInScheduleId, subgroupId }: NewSubgroupLesson) => {
        try {
            const subgroupLesson = new SubgroupLessonValue();
            subgroupLesson.setValues(subgroupId, lessonInScheduleId);
            const valueInDb = (await this.controller.find(subgroupLesson))[0];
            if (valueInDb) {
                return valueInDb;
            }
            await this.controller.add(subgroupLesson);
            return (await this.controller.find(subgroupLesson))[0];
        } catch (e) {
            throw new ApolloError(e, "500");
        }
    };

    private subgroupController = new Controller<SubgroupType>(SubgroupValue);
    private subgroupLogic = new SubgroupLogic();
    private lessonInScheduleLogic = new LessonInScheduleLogic();
}

export default SubgroupLessonLogic;

import * as mocha from 'mocha';
import {LessonInScheduleType, SubgroupLessonType} from "../../Models";
import { reservedLessonsInSchedule, reservedSubgroupLessonsData as dataForSend } from "../utils.spec";

const assert = require("assert");
import SubgroupLessonLogic from "../SubgroupLesson";
import LessonInScheduleLogic from "../LessonInSchedule";
import {ApolloError} from "apollo-server-errors";

describe('Subgroup Lesson Logic logic test', () => {
    const subgroupLessonLogic = new SubgroupLessonLogic();
    const lessonInScheduleLogic = new LessonInScheduleLogic();
    let lessonInSchedule: LessonInScheduleType;
    let subgroupLesson: SubgroupLessonType;


    mocha.before(async () => {
        lessonInSchedule = await lessonInScheduleLogic.addLessonInSchedule(reservedLessonsInSchedule[0], 1);
    });

    it('should return false', async function () {
        const lessonInScheduleId = 13;
        await subgroupLessonLogic.deleteUserLessonInSchedule(lessonInScheduleId);
        const shouldBeUndefined = (await subgroupLessonLogic.getSubgroupLessonsByLessonInScheduleIds([13]))[0];
        assert.strictEqual(shouldBeUndefined, undefined);
    });

    it('should create a subgroup lesson', async function () {
        try {
            subgroupLesson = await subgroupLessonLogic.insertIntoDb({
                lessonInScheduleId: lessonInSchedule.id,
                subgroupId: 10
            });
            assert.strictEqual(!!subgroupLesson.id, true);
        } catch (e) {
            throw new ApolloError(e);
        }
    });

    //TODO too slow test
    it('should add one subgroupLesson and find this in db', async function () {
        let res = true;
        const lesson = dataForSend.lessons[0];
        try {
            const sgLesson = await subgroupLessonLogic.addSubgroupLessonToDb(lesson, 1);
            const lis = sgLesson.getLessonInSchedule;
            if (lis.getIsChisl !== lesson.isChisl
                || sgLesson.getSubgroupId !== lesson.subgroupId
                || lis.getWeekIndex !== lesson.weekIndex
                || lis.getDayIndex !== lesson.dayIndex
                || lis.lessonId !== lesson.lessonId) {
                res = false;
            }
        } catch (e) {
            console.log(e);
            res = false;
        }
        assert(res, true);
    });

    it('should not create same subgroup lesson', async function () {
        try {
            const sameSubgroupLesson = await subgroupLessonLogic.insertIntoDb({
                lessonInScheduleId: lessonInSchedule.id,
                subgroupId: 10
            });
            assert.strictEqual(sameSubgroupLesson.id, subgroupLesson.id);
        } catch (e) {
            throw new ApolloError(e);
        }
    });

    mocha.after(async function() {
        await subgroupLessonLogic.deleteUserLessonInSchedule(lessonInSchedule.id)
    });
});

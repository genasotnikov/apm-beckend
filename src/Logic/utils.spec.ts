export const reservedLessonsInSchedule = [
    {dayIndex: 6, weekIndex: 6, isChisl: true, lessonId: 24},
    {dayIndex: 6, weekIndex: 6, isChisl: true, lessonId: 20}
];

export const reservedSubgroupLessonsData = { lessons:[
        { isChisl: true, weekIndex:0, dayIndex:0, subgroupId:10, lessonId:20},
        { isChisl: false, weekIndex:0, dayIndex:0, subgroupId:10, lessonId:20},
        { isChisl: false, weekIndex:0, dayIndex:1, subgroupId:10, lessonId:24},
        { isChisl: false, weekIndex:0, dayIndex:2, subgroupId:10, lessonId:22},
        { isChisl: false, weekIndex:0, dayIndex:4, subgroupId:10, lessonId:22}]
};

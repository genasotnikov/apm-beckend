let assert = require("assert");
import SemesterLogic from "../Semester";

describe('Semester logic test test', () => {
    const semesterLogic = new SemesterLogic();
    it("should find semester by args", async () => {
        const foundSemester = await semesterLogic.findSemester({ isFirstSemester: true, year: 2020 });
        assert(foundSemester.isFirstSemester && foundSemester.year === 2020, true);
    });
});

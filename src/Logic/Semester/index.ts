import {SemesterType, SemesterValue} from "../../Models";
import CRUD from "../CRUD";
import {ApolloError} from "apollo-server-errors";

interface findSemesterArgs {
    year: number
    isFirstSemester: boolean
}

class SemesterLogic extends CRUD<SemesterType>{
    constructor() {
        super(SemesterValue);
    }

    async findSemester(findSemesterArgs: findSemesterArgs): Promise<SemesterType> {
        try {
            return (await this.controller.find(findSemesterArgs))[0];
        } catch (e) {
            throw new ApolloError("Internal", "500");
        }
    }

    getSemesterIdByYearAndMonth = async (year: number, monthIndex: number) => {
         const semester = (await this.controller.find({
            year,
            isFirstSemester: [0, 8, 9, 10, 11].some(el => el === Number(monthIndex))
        } as SemesterType))[0];

        return semester && semester.id;
    };
}

export default SemesterLogic;

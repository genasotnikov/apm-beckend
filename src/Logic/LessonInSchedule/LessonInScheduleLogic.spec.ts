import {LessonInScheduleType} from "../../Models";
import { reservedLessonsInSchedule } from "../utils.spec";
import SubgroupLessonLogic from "../SubgroupLesson";
import LessonInScheduleLogic from "../LessonInSchedule";
import {ApolloError} from "apollo-server-errors";

let assert = require("assert");

describe('Lesson in schedule logic test', () => {
    const lessonInScheduleLogic = new LessonInScheduleLogic();
    const subgroupLessonLogic = new SubgroupLessonLogic();
    let foundLessonInScheduleId = -1;
    let newLessonInSchedule: LessonInScheduleType;

    before(async () => {
        try {
            const firstReservedInDb = (await lessonInScheduleLogic.findLessonsInSchedule(reservedLessonsInSchedule[0]))[0];
            const secondReservedInDb = (await lessonInScheduleLogic.findLessonsInSchedule(reservedLessonsInSchedule[1]))[0];
            if (firstReservedInDb?.id) await subgroupLessonLogic.deleteUserLessonInSchedule(firstReservedInDb.id);
            if (secondReservedInDb?.id) await subgroupLessonLogic.deleteUserLessonInSchedule(secondReservedInDb.id);
        } catch (e) {
            console.log(e);
            throw new ApolloError(e);
        }

    });


    it('should return lessons in schedule of selected day, selected schedule (chisl or znam)', async function () {
        let testRes = true;
        const functionRes = await lessonInScheduleLogic.getDayLessonsInSchedule(1, 0);

        functionRes.forEach((el: LessonInScheduleType) => {
            if (el.getWeekIndex !== 0 || el.getscheduleId !== 1) {
                testRes = false;
            }
        });

        assert.strictEqual(testRes, true)
    });

    it('should create lesson in schedule', async function () {
        const newLessonInSchedule1 = await lessonInScheduleLogic.addLessonInSchedule(reservedLessonsInSchedule[0], 1);
        const foundLessonInSchedule = (await lessonInScheduleLogic.findLessonsInSchedule({id: newLessonInSchedule1.id}))[0];
        foundLessonInScheduleId = foundLessonInSchedule.id;
        assert.strictEqual(newLessonInSchedule1.id, foundLessonInScheduleId);
    });

    it('should update and find lessonInSchedule by args', async function () {
        let res = true;
        newLessonInSchedule = await lessonInScheduleLogic.addLessonInSchedule(reservedLessonsInSchedule[1], 1);
        const foundLisByArgs = (await lessonInScheduleLogic.findLessonsInSchedule(reservedLessonsInSchedule[1]))[0];
        if (foundLisByArgs.getWeekIndex !== reservedLessonsInSchedule[1].weekIndex
            || foundLisByArgs.getIsChisl !== reservedLessonsInSchedule[1].isChisl
            || foundLisByArgs.getDayIndex !== reservedLessonsInSchedule[1].dayIndex
            || foundLisByArgs.lessonId !== reservedLessonsInSchedule[1].lessonId)
            res = false;
        assert(res, true);
    });

    it('should delete created lesson in schedule', async function () {
        await lessonInScheduleLogic.deleteLessonInScheduleById(foundLessonInScheduleId);
        const foundLessonInSchedule = (await lessonInScheduleLogic.findLessonsInSchedule({id: newLessonInSchedule.id}))[0];
        assert.strictEqual(foundLessonInSchedule, undefined);
    });
});

import {
    LessonInScheduleType,
    LessonInScheduleValue,
    LessonType,
    LessonTypeType,
    LessonTypeValue,
    LessonValue
} from "../../Models";
import ScheduleLogic from "../Schedule";
import Controller from "../../services/Controller";
import {ApolloError} from "apollo-server-errors";
import {AddLessonInScheduleTypeArgs} from "../../resolvers/LessonInSchedule";

class LessonInScheduleLogic {

    getDayLessonsInSchedule = async (scheduleId: number, weekIndex: number) => {
        const lessonsInSchedule = await this.controller.find({ scheduleId, weekIndex });
        return lessonsInSchedule;
    };

    deleteLessonInScheduleById = async (id: number) => {
        try {
            await this.controller.deleteByIds([id]);
        }
        catch (e) {
            throw new ApolloError(e);
        }
    };

    async getLessonInSchedule(resInDb: LessonInScheduleType):Promise<LessonInScheduleType> {
        const lesson = (await this.lessonController.findByIds([resInDb.lessonId]))[0];
        const lessonType = (await this.lessonTypeController.findByIds([lesson.typeId]))[0];
        lesson.setLessonType(lessonType);
        resInDb.setLesson(lesson);
        const sem = await this.scheduleLogic.getScheduleById(resInDb.getscheduleId)
        resInDb.setSchedule(sem);
        return resInDb;
    }

    async addLessonInSchedule( { dayIndex, lessonId, weekIndex, isChisl }: AddLessonInScheduleTypeArgs, userId: number) {
        try {
            const scheduleId = (await this.scheduleLogic.getCurrentSchedule(userId)).id;
            const newLessonInSchedule = new LessonInScheduleValue();
            newLessonInSchedule.setValue(dayIndex, weekIndex, lessonId, scheduleId, isChisl);
            const lisAlreadyInDb = (await this.controller.find({dayIndex, weekIndex, scheduleId, isChisl}))[0];
            if (lisAlreadyInDb) {
                await this.controller.update(lisAlreadyInDb.id, newLessonInSchedule);
                const newValueInDb = (await this.controller.find({dayIndex, weekIndex, scheduleId, isChisl}))[0];
                return await this.getLessonInSchedule(newValueInDb);
            } else {
                await this.controller.add(newLessonInSchedule);
                const resInDb = (await this.controller.find(newLessonInSchedule))[0];
                return await this.getLessonInSchedule(resInDb);
            }
        } catch (e) {
            throw new ApolloError(e);
        }
    }

    async findLessonsInSchedule(args: Object): Promise<LessonInScheduleType[]> {
        const lessonsInScheduleInDb:LessonInScheduleType[] = await this.controller.find({...args});
        let a;
        for (a of lessonsInScheduleInDb) {
            a = await this.getLessonInSchedule(a);
        }
        return lessonsInScheduleInDb;
    }

    private scheduleLogic = new ScheduleLogic();
    protected lessonTypeController = new Controller<LessonTypeType>(LessonTypeValue);
    protected lessonController = new Controller<LessonType>(LessonValue);
    private controller = new Controller<LessonInScheduleType>(LessonInScheduleValue)
}

export default LessonInScheduleLogic;

import { ApolloError } from "apollo-server-errors";
import {EntitySchema} from "typeorm";
import { connectToDB } from "../../config/mysql";
import {PaginatedInputType} from "../Models";


interface ControllerInterface<TableRow> {
    add: (value:TableRow) => Promise<void | Error>,
    find: (args:object) => Promise<any[]>,
    findByIds: (ids: number[]) => Promise<any[]>,
    update: (id: number, value: TableRow) => Promise<void | Error>,
    deleteByIds: (ids: number[]) => Promise<void | Error>,
}

type repository = string | Function | (new () => unknown) | EntitySchema<unknown>;

class Controller<Table> implements ControllerInterface<Table> {
    constructor(t: unknown) {
        this.table = t;
    }

    async getPaginatedData(paginatedInput: PaginatedInputType): Promise<Table[]> {
        const connection = await connectToDB();
        const result = await connection
            .getRepository(this.table as repository)
            .find({ take: paginatedInput.limit, where: `id>${paginatedInput.lastIndex}`, order: { id: "ASC" } });
        await connection.close();
        return (result as unknown as Table[]);
    }

    public async find(args?: object): Promise<Table[]> {
        const connection = await connectToDB();
        const foundValues = await connection.getRepository(this.table as repository).find({ where: args }) as Table[];
        await connection.close();
        return foundValues;
    }

    public async findByIds(ids: number[]): Promise<Table[]>  {
        const connection = await connectToDB();
        const foundValues = await connection.getRepository(this.table as repository).findByIds(ids) as Table[];
        await connection.close();
        return foundValues;
    }

    public async findByFKIds(fkIdColumnName: string,ids: number[]): Promise<Table[]>  {
        const connection = await connectToDB();
        const rep = await connection.getRepository(this.table as repository);
        const foundValues = await rep
            .createQueryBuilder(this._repName)
            .where(`${this._repName}.${fkIdColumnName} IN (:fkIds)`, { fkIds: ids })
            .getMany() as Table[];
        await connection.close();
        return foundValues;
    }

    public async add(value: Table) {
        try {
            const connection = await connectToDB();
            const rep = connection.getRepository(this.table as repository)
            await rep.save(value);
            await connection.close();
        } catch (e) {
            throw new ApolloError(e, "500");
        }
    }

    public async update(id: number, value: Table) {
        try {
            const connection = await connectToDB();
            await connection.getRepository(this.table as repository).update({ id }, value);
            await connection.close();
        } catch (e) {
            throw new ApolloError(e, "500");
        }
    }

    public async deleteByIds(ids: number[]) {
        try {
            const connection = await connectToDB();
            for (const id of ids) {
                await connection.getRepository(this.table as repository).delete({ id });
            }
            await connection.close();
        } catch (e) {
            throw new ApolloError(e, "500");
        }
    }

    public async getCount(): Promise<number> {
        const connection = await connectToDB();
        const count = await connection.getRepository(this.table as repository).count();
        await connection.close();
        return count;
    }

    public async query(query: string):Promise<any[]> {
        const connection = await connectToDB();
        const foundValues = await connection.query(query);
        await connection.close();
        return foundValues;
    }

    set repName(v:string) {
        this._repName = v;
    }

    private _repName: string;
    private table: unknown;
}

export default Controller;

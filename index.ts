import "reflect-metadata";
import createError from "http-errors";
import express from "express";
import jwt from "express-jwt";
import fs from "fs";
import pathLib from "path";
import ApolloServer from "./config/graphqlServer";
import cors from "cors";

const corsOptions = {
  origin: "http://localhost:3000",
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

const app = express();
app.use(cors(corsOptions));
const path = "/graphql";
const GQL_PORT = 4000;

ApolloServer.applyMiddleware({ app, path });
app.get("/loadVedomost/:filename", (req, res) => {
  const filePath = pathLib.resolve("./tempFiles/" + req.params.filename);
  res.sendFile(filePath);
});
app.listen({ port: GQL_PORT }, () =>
  console.log(
    `🚀 Server ready at http://localhost:4000${ApolloServer.graphqlPath}`
  )
);

app.use(
  path,
  jwt({
    secret: "TypeGraphQL",
    credentialsRequired: false,
  })
);

app.use(express.json());

// enable cors
/*let corsOptions = {
    origin: '<insert uri of front-end domain>',
    credentials: true // <-- REQUIRED backend setting
};*/
// app.use(cors(corsOptions));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

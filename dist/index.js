"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const http_errors_1 = __importDefault(require("http-errors"));
const express_1 = __importDefault(require("express"));
const express_jwt_1 = __importDefault(require("express-jwt"));
const path_1 = __importDefault(require("path"));
const graphqlServer_1 = __importDefault(require("./config/graphqlServer"));
const cors_1 = __importDefault(require("cors"));
const corsOptions = {
    origin: "http://localhost:3000",
    optionsSuccessStatus: 200,
};
const app = express_1.default();
app.use(cors_1.default(corsOptions));
const path = "/graphql";
const GQL_PORT = 4000;
graphqlServer_1.default.applyMiddleware({ app, path });
app.get("/loadVedomost/:filename", (req, res) => {
    const filePath = path_1.default.resolve("./tempFiles/" + req.params.filename);
    res.sendFile(filePath);
});
app.listen({ port: GQL_PORT }, () => console.log(`🚀 Server ready at http://localhost:4000${graphqlServer_1.default.graphqlPath}`));
app.use(path, express_jwt_1.default({
    secret: "TypeGraphQL",
    credentialsRequired: false,
}));
app.use(express_1.default.json());
// enable cors
/*let corsOptions = {
    origin: '<insert uri of front-end domain>',
    credentials: true // <-- REQUIRED backend setting
};*/
// app.use(cors(corsOptions));
// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(http_errors_1.default(404));
});
//# sourceMappingURL=index.js.map
import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class lessonTypes21593117749662 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn("lessons", "time");
        await queryRunner.addColumn("lesson_types", new TableColumn({
            name: "time",
            type: "float(4,2)",
            isNullable: false,
            default: 2
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn("lesson_types", "time");
        await queryRunner.addColumn("lessons", new TableColumn({
            name: "time",
            type: "float(4,2)",
            isNullable: false,
        }));
    }

}

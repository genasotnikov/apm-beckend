import {MigrationInterface, QueryRunner, TableForeignKey, TableIndex} from "typeorm";
import LessonsMigrationService from "../migrationService/LessonsMigrationService";

export class lessonInSchedule31593985483834 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropForeignKey(this.tableName, "LessonLessonId");
        await queryRunner.dropForeignKey(this.tableName, "lessons_in_schedule_semester");
        await queryRunner.dropIndex(this.tableName, "one_lesson");
        await queryRunner.renameColumn(this.tableName, "semesterId", "scheduleId");
        await queryRunner.renameTable("schedule", "schedules");
        await queryRunner.createForeignKey(this.tableName, new TableForeignKey({
            name: "schedule_fk",
            columnNames: ["scheduleId"],
            referencedTableName: "schedules",
            referencedColumnNames: ["id"],
        }));
        const MigrationService = new LessonsMigrationService(queryRunner);
        await MigrationService.groupIdAndLessonIdFKs();
        await queryRunner.createIndex(this.tableName, new TableIndex({
            name: "one_lesson",
            columnNames: ["scheduleId", "dayIndex", "weekIndex", "isChisl"],
            isUnique: true,
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropForeignKey(this.tableName, "LessonLessonId");
        await queryRunner.dropForeignKey(this.tableName, "schedule_fk");
        await queryRunner.dropIndex(this.tableName, "one_lesson");
        await queryRunner.renameTable("schedules", "schedule");
        await queryRunner.renameColumn(this.tableName, "scheduleId", "semesterId");
        const MigrationService = new LessonsMigrationService(queryRunner);
        await MigrationService.groupIdAndLessonIdFKs();
        await queryRunner.createForeignKey("lessons_in_schedule", new TableForeignKey({
            referencedTableName: "semesters",
            name: "lessons_in_schedule_semester",
            columnNames: ["semesterId"],
            referencedColumnNames: ["id"],
        }));
        await queryRunner.createIndex("lessons_in_schedule",new TableIndex({
            name: "one_lesson",
            columnNames: ["dayIndex", "weekIndex", "semesterId"],
            isUnique: true
        }));
    }

    tableName = "lessons_in_schedule";
}

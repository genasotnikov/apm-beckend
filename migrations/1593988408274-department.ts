import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey, TableIndex} from "typeorm";
import {initialDepartment} from "../queries";

export class department1593988408274 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: this.tableName,
            columns: [
                new TableColumn({
                    name: "name",
                    isPrimary: true,
                    type: "varchar",
                }),
                new TableColumn({
                    name: "director",
                    type: "varchar",
                    isNullable: true,
                })
            ],
        }));
        await queryRunner.query(initialDepartment);
        await queryRunner.addColumn("user", new TableColumn({
            name: "department",
            default: `'Кафедра информатики и МПМ'`,
            type: "varchar"
        }));
        await queryRunner.createForeignKey("user", new TableForeignKey({
            name: "department_fk",
            referencedColumnNames: ["name"],
            referencedTableName: this.tableName,
            columnNames: ["department"]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropForeignKey("user", "department_fk");
        await queryRunner.dropColumn("user", "department");
        await queryRunner.dropTable(this.tableName);
    }

    tableName = "department";
}

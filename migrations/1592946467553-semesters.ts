import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";
import { initialSemesters } from "../queries";

export class semesters1592946467553 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const Semesters = new Table({
            name: "semesters",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment",
                },
                {
                    name: "year",
                    type: "int",
                    isNullable: false
                },
                {
                    name: "isFirstSemester",
                    type: "boolean",
                    isNullable: false
                },]});
        Semesters.addIndex(new TableIndex({
            name: "semesterIndex",
            columnNames: ["isFirstSemester", "year"],
            isUnique: true
        }), true);
        await queryRunner.createTable(Semesters);
        await queryRunner.query(initialSemesters);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropIndex("semesters", "semesterIndex");
        await queryRunner.dropTable("semesters");
    }
}

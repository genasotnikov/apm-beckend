import {MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex} from "typeorm";
import {initialGroupes} from "../queries";

export class groupes1591483548023 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const Groups = new Table({
            name: "groups",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment",
                },
                {
                    name: "profileId",
                    type: "int",
                },
                {
                    name: "year",
                    type: "int",
                },
                {
                    name: "number",
                    type: "int",
                },
            ],
        });
        await queryRunner.createTable(Groups);
        await queryRunner.createForeignKey("groups", new TableForeignKey({
            columnNames: ["profileId"],
            referencedColumnNames: ["id"],
            referencedTableName: "profiles",
            onDelete: "CASCADE",
            name: "profileId_fk"
        }));
        await queryRunner.createIndex("groups", new TableIndex({
            name: "unique_group",
            columnNames: ["profileId", "year", "number"],
            isUnique: true
        }));
        await queryRunner.query(initialGroupes);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("groups");
        if (table) {
            const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("profileId") !== -1);
            await queryRunner.dropForeignKey("groups", foreignKey);
            await queryRunner.dropTable("groups", true);
        }
    }

}

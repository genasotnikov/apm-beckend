import {MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex} from "typeorm";
import {initialTypes} from "../queries";

export class lessons1591643626599 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        const Lessons = new Table({
            name: "lessons",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment",
                },
                {
                    name: "teacherId",
                    type: "int",
                },
                {
                    name: "content",
                    type: "varchar",
                    isNullable: false
                },
                {
                    name: "typeId",
                    type: "int",
                    isNullable: false
                },
            ],
        });
        await queryRunner.createTable(Lessons);
        await queryRunner.createForeignKey("lessons", new TableForeignKey({
            columnNames: ["teacherId"],
            referencedColumnNames: ["id"],
            referencedTableName: "User",
            onDelete: "CASCADE"
        }));
        await queryRunner.createForeignKey("lessons", new TableForeignKey({
            columnNames: ["typeId"],
            referencedColumnNames: ["id"],
            referencedTableName: "lesson_types",
            onDelete: "CASCADE"
        }));
        await queryRunner.createIndex("lessons", new TableIndex({
            name: "unique_lesson",
            columnNames: ["teacherId", "content", "typeId"],
            isUnique: true,
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("lessons");
        if (table) {
            const foreignKeys = new Array<TableForeignKey>();
            foreignKeys[0] = table.foreignKeys.find(fk => fk.columnNames.indexOf("teacherId") !== -1);
            foreignKeys[1] = table.foreignKeys.find(fk => fk.columnNames.indexOf("typeId") !== -1);
            for (const fk of foreignKeys) {
                await queryRunner.dropForeignKey("lessons", fk);
            }
            await queryRunner.dropTable("lessons", true);
        }
    }

}

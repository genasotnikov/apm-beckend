import {MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class lessonTime1592145120967 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn("lessons", new TableColumn({
            name: "time",
            type: "float(4,2)",
            isNullable: false,
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn("lessons", "time");
    }
}

import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey, TableIndex} from "typeorm";

export class subgroupLesson1593811865813 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: this.tableName,
            columns: [
                new TableColumn({
                    name: "id",
                    isPrimary: true,
                    isGenerated: true,
                    type: "int",
                    generationStrategy: "increment"
                }),
                new TableColumn({
                    name: "subgroup_id",
                    type: "int",
                }),
                new TableColumn({
                    name: "lesson_in_schedule_id",
                    type: "int",
                }),
            ]
        }));

        await queryRunner.createForeignKey(this.tableName, new TableForeignKey({
            name: "lesson_in_schedule_fk",
            columnNames: ["lesson_in_schedule_id"],
            referencedTableName:"lessons_in_schedule",
            referencedColumnNames: ["id"],
        }));

        await queryRunner.createForeignKey(this.tableName, new TableForeignKey({
            name: "subgroup_id_fk",
            columnNames: ["subgroup_id"],
            referencedTableName:"subgroups",
            referencedColumnNames: ["id"],
        }));

        await queryRunner.createIndex(this.tableName, new TableIndex({
            name: "unique_subgroup_lesson",
            columnNames: ["lesson_in_schedule_id", "subgroup_id"],
            isUnique: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropForeignKey(this.tableName, "lesson_in_schedule_fk");
        await queryRunner.dropForeignKey(this.tableName, "subgroup_id_fk");

        await queryRunner.dropIndex(this.tableName, "unique_subgroup_lesson");
        await queryRunner.dropTable(this.tableName, true);
    }

    private tableName:string = "subgroup_lesson"
}

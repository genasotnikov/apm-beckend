import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey, TableIndex} from "typeorm";

export class isChisl1593895659686 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn(this.tableName, new TableColumn({
            name: "isChisl",
            type: "boolean",
            default: true
        }));
        await queryRunner.dropForeignKey(this.tableName, "lessons_in_schedule_semester");
        await queryRunner.dropIndex(this.tableName, "one_lesson");
        await queryRunner.createForeignKey("lessons_in_schedule", new TableForeignKey({
            referencedTableName: "semesters",
            name: "lessons_in_schedule_semester",
            columnNames: ["semesterId"],
            referencedColumnNames: ["id"],
        }));
        await queryRunner.createIndex(this.tableName, new TableIndex({
            name: "one_lesson",
            columnNames: ["dayIndex", "weekIndex", "semesterId", "isChisl"],
            isUnique: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropForeignKey(this.tableName, "lessons_in_schedule_semester");
        await queryRunner.dropIndex(this.tableName, "one_lesson");
        await queryRunner.createForeignKey("lessons_in_schedule", new TableForeignKey({
            referencedTableName: "semesters",
            name: "lessons_in_schedule_semester",
            columnNames: ["semesterId"],
            referencedColumnNames: ["id"],
        }));
        await queryRunner.createIndex(this.tableName, new TableIndex({
            name: "one_lesson",
            columnNames: ["dayIndex", "weekIndex", "semesterId"],
            isUnique: true
        }));
        await queryRunner.dropColumn(this.tableName, "isChisl");
    }

    tableName = "lessons_in_schedule";
}

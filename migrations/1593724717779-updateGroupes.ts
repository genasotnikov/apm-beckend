import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey, TableIndex} from "typeorm";
import {initialEduForms, initialEduTypes} from "../queries";

export class updateGroupes1593724717779 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        const TypesOfEdu = new Table({
           name: "types_of_edu",
           columns:[{
               name: "name",
               isPrimary: true,
               type: "varchar"
           }]
        });
        await queryRunner.createTable(TypesOfEdu);

        const FormsOfEdu = new Table({
            name: "forms_of_edu",
            columns:[{
                name: "name",
                isPrimary: true,
                type: "varchar"
            }]
        });
        await queryRunner.createTable(FormsOfEdu);
        await queryRunner.query(initialEduForms);
        await queryRunner.query(initialEduTypes);
        await queryRunner.addColumns("groups", [
            new TableColumn({
                name: "type_of_edu",
                type: "varchar",
                default: `'БАКАЛАВРИАТ'`
            }),
            new TableColumn({
                name: "form_of_edu",
                type: "varchar",
                default: `'ОЧНАЯ'`
            }),
        ]);
        await queryRunner.createForeignKeys("groups", [
            new TableForeignKey({
                columnNames: ["form_of_edu"],
                referencedColumnNames: ["name"],
                referencedTableName: "forms_of_edu",
                onDelete: "CASCADE",
                name: "edy_forms_fk"
            }),
            new TableForeignKey({
                columnNames: ["type_of_edu"],
                referencedColumnNames: ["name"],
                referencedTableName: "types_of_edu",
                onDelete: "CASCADE",
                name: "edy_types_fk"
            }),
        ]);
        await queryRunner.dropIndex("groups", "unique_group");
        await queryRunner.createIndex("groups", new TableIndex({
            name: "unique_group",
            columnNames: ["form_of_edu", "type_of_edu", "year", "profileId", "number"],
            isUnique: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropForeignKey(this.tableName, "edy_types_fk");
        await queryRunner.dropForeignKey(this.tableName, "edy_forms_fk");
        await queryRunner.dropForeignKey(this.tableName, "profileId_fk");
        await queryRunner.dropIndex("groups", "unique_group");
        await queryRunner.createIndex("groups", new TableIndex({
            name: "unique_group",
            columnNames: ["year", "profileId", "number"],
            isUnique: true
        }));
        await queryRunner.createForeignKey(this.tableName, new TableForeignKey({
            name: "profileId_fk",
            columnNames: ["profileId"],
            referencedTableName: "profiles",
            referencedColumnNames: ["id"]
        }));
        await queryRunner.dropColumn(this.tableName, "type_of_edu");
        await queryRunner.dropColumn(this.tableName, "form_of_edu");


        await queryRunner.dropTable("types_of_edu");
        await queryRunner.dropTable("forms_of_edu");
    }

    private tableName = "groups";
}

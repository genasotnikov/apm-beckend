import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey, TableIndex} from "typeorm";

export class subgroups1593808369879 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: this.tableName,
            columns: [
                new TableColumn({
                    name: "id",
                    isPrimary: true,
                    isGenerated: true,
                    type: "int",
                    generationStrategy: "increment"
                }),
                new TableColumn({
                    name: "number",
                    type: "int",
                }),
                new TableColumn({
                    name: "groupId",
                    type: "int",
                }),
            ]
        }));

        await queryRunner.createForeignKey(this.tableName, new TableForeignKey({
            name: "group_fk",
            columnNames: ["groupId"],
            referencedTableName:"groups",
            referencedColumnNames: ["id"],
        }));

        await queryRunner.createIndex(this.tableName, new TableIndex({
            name: "unique_group",
            columnNames: ["groupId", "number"],
            isUnique: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropForeignKey(this.tableName, "group_fk");
        await queryRunner.dropIndex(this.tableName, "unique_group");
        await queryRunner.dropTable(this.tableName, true);
    }

    private tableName:string = "subgroups"
}

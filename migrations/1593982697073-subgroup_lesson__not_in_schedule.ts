import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey, TableIndex} from "typeorm";

export class subgroupLesson_notInSchedule1593982697073 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: this.tableName,
            columns: [
                new TableColumn({
                    name: "id",
                    isPrimary: true,
                    isGenerated: true,
                    type: "int",
                    generationStrategy: "increment"
                }),
                new TableColumn({
                    name: "subgroup_id",
                    type: "int",
                }),
                new TableColumn({
                    name: "lesson_not_in_schedule_id",
                    type: "int",
                }),
            ],
            indices: [
                new TableIndex({
                    name: "unique_subgroup_lesson",
                    columnNames: ["lesson_not_in_schedule_id", "subgroup_id"],
                    isUnique: true
                })
            ],
            foreignKeys: [
                new TableForeignKey({
                    name: "lesson_not_in_schedule_fk",
                    columnNames: ["lesson_not_in_schedule_id"],
                    referencedTableName:"lessons_not_in_schedule",
                    referencedColumnNames: ["id"],
                }),
                new TableForeignKey({
                    name: "subgroup_ns_id_fk",
                    columnNames: ["subgroup_id"],
                    referencedTableName:"subgroups",
                    referencedColumnNames: ["id"],
                })
            ]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable(this.tableName, true);
    }

    private tableName:string = "subgroup_lesson_not_in_schedule"
}

import {MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex} from "typeorm";
import LessonsMigrationService from "../migrationService/LessonsMigrationService";

export class lessonsInSchedule1592037047471 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const LessonsInSchedule = new Table({
            name: "lessons_in_schedule",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment",
                },
                {
                    name: "lessonId",
                    type: "int",
                    isNullable: false
                },
                {
                    name: "groupId",
                    type: "int",
                    isNullable: false
                },
                {
                    name: "dayIndex",
                    type: "int",
                    isNullable: false
                },
                {
                    name: "weekIndex",
                    type: "int",
                    isNullable: false
                }]});
        await queryRunner.createTable(LessonsInSchedule);
        const MigrationService = new LessonsMigrationService(queryRunner);
        await MigrationService.groupIdAndLessonIdFKs();
        await queryRunner.createIndex("lessons_in_schedule", new TableIndex({
            name: "one_lesson",
            columnNames: ["groupId", "dayIndex", "weekIndex"],
            isUnique: true,
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const MigrationService = new LessonsMigrationService(queryRunner);
        await MigrationService.deleteGroupIdAndLessonIdFKs();
        await queryRunner.dropTable("lessons_in_schedule", true);
    }
}

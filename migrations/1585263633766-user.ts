import {MigrationInterface, Table, QueryRunner} from "typeorm";

// @ts-ignore
export class user1585263633766 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: "User",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment",
                },
                {
                    name: "email",
                    type: "varchar",
                    isUnique: true,
                    isNullable: true,
                },
                {
                    name: "password",
                    type: "varchar",
                    isNullable: true
                },
                {
                    name: "socialToken",
                    type: "varchar",
                    isUnique: true,
                    isNullable: true
                },
                {
                    name: "firstName",
                    type: "varchar",
                },
                {
                    name: "middleName",
                    type: "varchar",
                    isNullable: true
                },
                {
                    name: "lastName",
                    type: "varchar",
                },
            ]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable("User");
    }
}

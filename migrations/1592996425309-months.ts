import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";
import {initialMonths} from "../queries";

export class months1592996425309 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const Months = new Table({
            name: "months",
            columns: [
                {
                    name: "name",
                    isPrimary: true,
                    type: "varchar",
                },
                {
                    name: "isFirstSemester",
                    type: "boolean",
                    isNullable: false
                },]});
        await queryRunner.createTable(Months);
        await queryRunner.query(initialMonths);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable("months");
    }

}

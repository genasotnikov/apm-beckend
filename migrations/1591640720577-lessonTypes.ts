import {MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex} from "typeorm";
import { initialTypes } from "../queries";
export class lessonTypes1591640720577 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const LessonTypes = new Table({
            name: "lesson_types",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment",
                },
                {
                    name: "teacherId",
                    type: "int",
                },
                {
                    name: "name",
                    type: "varchar",
                    isNullable: false
                }
            ],
        });
        await queryRunner.createTable(LessonTypes);
        await queryRunner.createForeignKey("lesson_types", new TableForeignKey({
            columnNames: ["teacherId"],
            referencedColumnNames: ["id"],
            referencedTableName: "User",
            onDelete: "CASCADE"
        }));
        await queryRunner.createIndex("lesson_types", new TableIndex({
            name: "unique_type",
            columnNames: ["name", "teacherId"],
            isUnique: true,
        }));
        await queryRunner.query(initialTypes);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("lesson_types");
        if (table) {
            const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("teacherId") !== -1);
            await queryRunner.dropForeignKey("lesson_types", foreignKey);
            await queryRunner.dropTable("lesson_types", true);
        }
    }

}

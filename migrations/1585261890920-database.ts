import {MigrationInterface, QueryRunner} from "typeorm";

export class Database1585261890920 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const dbs = await queryRunner.getDatabases();
        if (!dbs.includes("apm")) await queryRunner.createDatabase("apm", true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropDatabase("apm", true);
    }

}

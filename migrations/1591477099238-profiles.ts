import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";
import {initialProfiles} from "../queries";

export class profiles1591477099238 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const Profiles = new Table({
            name: "profiles",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment",
                },
                {
                    name: "name",
                    type: "varchar",
                    isUnique: true,
                },
                {
                    name: "shortness",
                    type: "varchar",
                    isUnique: true,
                    isNullable: true
                },
                {
                    name: "facultyId",
                    type: "int",
                }
            ],
        });
        await queryRunner.createTable(Profiles);
        await queryRunner.createForeignKey("profiles", new TableForeignKey({
            columnNames: ["facultyId"],
            referencedColumnNames: ["id"],
            referencedTableName: "faculties",
            onDelete: "CASCADE"
        }));
        await queryRunner.query(initialProfiles);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("profiles");
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("facultyId") !== -1);
        await queryRunner.dropForeignKey("profiles", foreignKey);
        await queryRunner.dropTable("profiles", true);
    }

}

import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey, TableIndex} from "typeorm";

export class overrideDays1593979081807 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: "override_days",
            columns: [
                new TableColumn({
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment"
                }),
                new TableColumn({
                    name: "date",
                    type: "DATE"
                }),
                new TableColumn({
                    name: "teacherId",
                    type: "int",
                })
            ],
            indices: [
                new TableIndex({
                    name: "unique_override_day",
                    isUnique: true,
                    columnNames: ["teacherId", "date"]
                })],
            foreignKeys: [new TableForeignKey({
                name: "teacher_fk",
                referencedColumnNames:["id"],
                referencedTableName: "user",
                columnNames: ["teacherId"]
            })]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable("override_days");
    }

}

import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey, TableIndex} from "typeorm";

export class lessonsNotInSchedule1593981622862 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: "lessons_not_in_schedule",
            columns: [
                new TableColumn({
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment"
                }),
                new TableColumn({
                    name: "lesson_id",
                    type: "int"
                }),
                new TableColumn({
                    name: "day_index",
                    type: "int"
                }),
                new TableColumn({
                    name: "day_id",
                    type: "int",
                })
            ],
            indices: [
                new TableIndex({
                    name: "unique_override_lesson",
                    isUnique: true,
                    columnNames: ["day_id", "day_index"]
                })],
            foreignKeys: [new TableForeignKey({
                    name: "day_fk",
                    referencedColumnNames:["id"],
                    referencedTableName: "override_days",
                    columnNames: ["day_id"]
                }),
                new TableForeignKey({
                    name: "lesson_fk",
                    referencedColumnNames:["id"],
                    referencedTableName: "lessons",
                    columnNames: ["lesson_id"]
                })
            ]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable("lessons_not_in_schedule");
    }

}

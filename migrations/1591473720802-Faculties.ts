import {MigrationInterface, Table, QueryRunner} from "typeorm";
import { initialFaculties } from "../queries";

export class Faculties1591473720802 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const Faculties = new Table({
            name: "Faculties",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment",
                },
                {
                    name: "name",
                    type: "varchar",
                    isUnique: true,
                },
                {
                    name: "shortness",
                    type: "varchar",
                    isUnique: true,
                    isNullable: true
                }]
        });
        await queryRunner.createTable(Faculties);
        await queryRunner.query(initialFaculties);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable("Faculties", true)
    }

}

import {MigrationInterface, QueryRunner, Table, TableIndex, TableForeignKey} from "typeorm";
import {initialMonths} from "../queries";

export class schedule1592999208971 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const Schedule = new Table({
            name: "schedule",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment",
                },
                {
                    name: "teacherId",
                    type: "int",
                },
                {
                    name: "semesterId",
                    type: "int",
                },
                {
                    name: "isFinished",
                    type: "boolean",
                    isNullable: false,
                    default: false
                }
            ]});
        Schedule.addIndex(new TableIndex({
            name: "scheduleIndex",
            columnNames: ["teacherId", "semesterId"],
            isUnique: true
        }), true);
        Schedule.addForeignKey(new TableForeignKey({
            name: "teacherForeignKey",
            columnNames: ["teacherId"],
            referencedTableName: "User",
            referencedColumnNames: ["id"],
        }));
        Schedule.addForeignKey(new TableForeignKey({
            name: "semesterForeignKey",
            columnNames: ["semesterId"],
            referencedTableName: "semesters",
            referencedColumnNames: ["id"],
        }));
        await queryRunner.createTable(Schedule);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("schedule");
        if (table) {
            const foreignKeys = new Array<TableForeignKey>();
            foreignKeys[0] = table.foreignKeys.find(fk => fk.columnNames.indexOf("teacherId") !== -1);
            foreignKeys[1] = table.foreignKeys.find(fk => fk.columnNames.indexOf("semesterId") !== -1);
            for (const fk of foreignKeys) {
                await queryRunner.dropForeignKey("schedule", fk);
            }
            await queryRunner.dropIndex("schedule", "scheduleIndex");
            await queryRunner.dropTable("schedule");
        }
    }
}

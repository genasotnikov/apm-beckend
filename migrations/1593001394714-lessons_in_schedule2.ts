import {MigrationInterface, QueryRunner, TableColumn, TableIndex, TableForeignKey} from "typeorm";
import LessonsMigrationService from "../migrationService/LessonsMigrationService";

export class lessonsInSchedule21593001394714 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        try {
            const MigrationService = new LessonsMigrationService(queryRunner);
            await MigrationService.deleteGroupIdAndLessonIdFKs();
            await queryRunner.dropIndex("lessons_in_schedule", "one_lesson");
            await queryRunner.dropColumn("lessons_in_schedule", "groupId");
            await queryRunner.addColumn("lessons_in_schedule", new TableColumn({
                name: "semesterId",
                type: "int",
                isNullable: false,
            }));
            await queryRunner.createForeignKey("lessons_in_schedule", new TableForeignKey({
                referencedTableName: "semesters",
                name: "lessons_in_schedule_semester",
                columnNames: ["semesterId"],
                referencedColumnNames: ["id"],
            }));
            await MigrationService.groupIdAndLessonIdFKs();
            await queryRunner.createIndex("lessons_in_schedule",new TableIndex({
                name: "one_lesson",
                columnNames: ["dayIndex", "weekIndex", "semesterId"],
                isUnique: true
            }));
        } catch (e) {
            throw new Error(e)
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable("lessons_in_schedule");
        if (table) {
            const foreignKeys = new Array<TableForeignKey>();
            foreignKeys[0] = table.foreignKeys.find(fk => fk.columnNames.indexOf("lessonId") !== -1);
            foreignKeys[1] = table.foreignKeys.find(fk => fk.columnNames.indexOf("semesterId") !== -1);
            for (const fk of foreignKeys) if (fk) await queryRunner.dropForeignKey("lessons_in_schedule", fk);
        }
        await queryRunner.dropIndex("lessons_in_schedule", "one_lesson");
        await queryRunner.dropColumn("lessons_in_schedule", "semesterId");
        await queryRunner.addColumn("lessons_in_schedule",new TableColumn({
            name: "groupId",
            type: "int",
            isNullable: false,
        }));
        const MigrationService = new LessonsMigrationService(queryRunner);
        await MigrationService.groupIdAndLessonIdFKs();
        await queryRunner.createIndex("lessons_in_schedule", new TableIndex({
            name: "one_lesson",
            columnNames: ["groupId", "dayIndex", "weekIndex"],
            isUnique: true,
        }));
    }
}

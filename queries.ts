export const initialFaculties = `INSERT INTO faculties(name, shortness)
VALUES ('Факультет гуманитарный',  'ГФ'),('Факультет иностранных языков', 'ФИЯ'),
        ('Факультет искусств и художественного образования', 'ФИиХО'), ('Факультет психолого-педагогический', 'ППФ'),
        ('Факультет физико-математический', 'ФМФ'),
       ('Факультет физической культуры и безопасности жизнедеятельности', 'ФФКиБЖД')`;

export const initialProfiles = `INSERT INTO profiles(name, shortness, facultyId)
VALUES ('Информатика и ИКТ',  'И', 5),('Математика и информатика', 'МИ', 5), ('Прикладная информатика', 'ПИ',5),
       ('Прикладная математика', 'ПМ',5),('Математика и физика', 'МиФ', 5)`;

export const initialGroupes = `INSERT INTO groups(profileId, year, number)
VALUES (1, 2016, 1), (2,2017,1), (2,2017, 2), (3, 2018, 1)`;

export const initialTypes = `INSERT INTO lesson_types(name, teacherId)
VALUES ('Лекции',1), ('Лабораторные работы',1), ('Курсовые работы',1), ('Консультации',1), ('Зачеты',1), ('Экзамены',1),
       ('Работа с аспирантами', 1)`;

export const initialSemesters = `INSERT INTO semesters(year, isFirstSemester)
VALUES ('2020',true), ('2020',false), ('2021',true), ('2021',false), ('2022',true), ('2022',false),
       ('2023',true), ('2023',false), ('2024',false), ('2024',true), ('2025',false),
       ('2025',true), ('2026',false), ('2026',true)`;

export const initialMonths = `INSERT INTO months(name, isFirstSemester)
VALUES ('Февраль',false), ('Март',false), ('Апрель',false), ('Май',false), ('Июнь',false),
       ('Сентябрь',true), ('Октябрь',true), ('Ноябрь',true), ('Декабрь',true), ('Январь',true)`;

export const initialEduTypes = `
    INSERT INTO types_of_edu VALUES ('БАКАЛАВРИАТ'), ('МАГИСТРАТУРА'), ('АСПИРАНТУРА')`;
export const initialEduForms = `INSERT INTO forms_of_edu VALUES ('ОЧНАЯ'), ('ЗАОЧНАЯ')`;

export const initialDepartment = `
    INSERT INTO department(name, director) VALUES ('Кафедра информатики и МПМ', 'Малев В.В.')`;

export const dayOverrideScheduleByDayId = (dayId: number) => (`
SELECT
    lessons_not_in_schedule.day_index as dayIndex,
    CONCAT(YEAR(CURRENT_DATE)-groups.year, profiles.shortness) as yearProfile,
    CONCAT(groups.number, '/',subgroups.number) as groupSubgroup,
    faculties.shortness as faculty,
    lessons.content,
    lesson_types.name as lessonType
FROM subgroup_lesson_not_in_schedule

    #subgroup info
    INNER JOIN subgroups
    on subgroup_id=subgroups.id

    #group info
    INNER JOIN groups
    on groups.id = subgroups.groupId

    #profile info
    INNER JOIN profiles
    on profiles.id = groups.profileId

    #faculty info
    INNER JOIN faculties
    on faculties.id = profiles.facultyId

    #lesson info
    INNER JOIN lessons_not_in_schedule
    on lesson_not_in_schedule_id=lessons_not_in_schedule.id

    #lesson info
    INNER JOIN lessons
    on lessons_not_in_schedule.lesson_id=lessons.id

    #lesson type info
    INNER JOIN lesson_types
    on lesson_types.id=lessons.typeId

WHERE day_id=${dayId}
`);

export const dayScheduleByWeekIndex = (weekIndex: number, scheduleId: number, isChisl: boolean) => (`
SELECT
    lessons_in_schedule.dayIndex,
    CONCAT(YEAR(CURRENT_DATE)-groups.year, profiles.shortness) as yearProfile,
    CONCAT(groups.number, '/',subgroups.number) as groupSubgroup,
    faculties.shortness as faculty,
    lessons.content,
    lesson_types.name as lessonType
FROM subgroup_lesson

    #subgroup info
    INNER JOIN subgroups
    on subgroup_id=subgroups.id

    #group info
    INNER JOIN groups
    on groups.id = subgroups.groupId

    #profile info
    INNER JOIN profiles
    on profiles.id = groups.profileId

    #faculty info
    INNER JOIN faculties
    on faculties.id = profiles.facultyId

    #lesson in schedule info
    INNER JOIN lessons_in_schedule
    on lesson_in_schedule_id=lessons_in_schedule.id

    INNER JOIN schedules
    ON lessons_in_schedule.scheduleId = schedules.id

    INNER JOIN semesters
    ON schedules.semesterId = semesters.id

    #lesson info
    INNER JOIN lessons
    on lessons_in_schedule.lessonId=lessons.id

    #lesson type info
    INNER JOIN lesson_types
    on lesson_types.id=lessons.typeId

WHERE weekIndex=${weekIndex} AND schedules.id = ${scheduleId} AND lessons_in_schedule.isChisl = ${isChisl}
`);

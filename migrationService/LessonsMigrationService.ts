import {QueryRunner, TableForeignKey} from "typeorm";

export default class LessonsMigrationService {
    constructor (queryRunner:QueryRunner) {
        this.queryRunner = queryRunner;
    }

    public async deleteGroupIdAndLessonIdFKs() {
        const table = await this.queryRunner.getTable(this.tableName);
        if (table) {
            const foreignKeys = new Array<TableForeignKey>();
            foreignKeys[0] = table.foreignKeys.find(fk => fk.columnNames.indexOf("lessonId") !== -1);
            foreignKeys[1] = table.foreignKeys.find(fk => fk.columnNames.indexOf("groupId") !== -1);
            for (const fk of foreignKeys) if (fk) await this.queryRunner.dropForeignKey(this.tableName, fk);
        }
    }

    public async groupIdAndLessonIdFKs() {
        /*await this.queryRunner.createForeignKey(this.tableName, new TableForeignKey({
            name: "LessonGroupId",
            columnNames: ["groupId"],
            referencedColumnNames: ["id"],
            referencedTableName: "groups",
            onDelete: "CASCADE"
        }));*/
        await this.queryRunner.createForeignKey(this.tableName, new TableForeignKey({
            name: "LessonLessonId",
            columnNames: ["lessonId"],
            referencedColumnNames: ["id"],
            referencedTableName: "lessons",
            onDelete: "CASCADE"
        }));
    }

    private queryRunner: QueryRunner;
    private tableName:string = "lessons_in_schedule";
}